﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SocialPlatforms;
using GooglePlayGames;

public class GoogleServices : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void GlobalAchivement()
    {

        if (Social.localUser.authenticated)
        {
            Debug.Log("User is authenticated");
            Social.ShowAchievementsUI();
        }
        else
        {
            Debug.Log("User is not authenticated");
        }
    }
    public void GlobalLeaderBoard()
    {

        if (Social.localUser.authenticated)
        {
            Debug.Log("User is authenticated");
            Social.ShowLeaderboardUI();
        }
        else
        {
            Debug.Log("User is not authenticated");
        }
    }
}
