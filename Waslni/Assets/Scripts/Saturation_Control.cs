﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.ImageEffects;
public class Saturation_Control : MonoBehaviour {

	/* private Variables المتغيرات الخاصة */


	/* public Variables المتغيرات العامة */
	public float start_value = 0.0F;
	public float final_value = 0.6F;
	public float step = 1; 
	public bool Saturation_Ctrl = true;
    public static bool active_LP = false;
    public static bool active_HP = false;
    // static variables متغيرات ثابته
    public static bool reset_sat = false;

    void Awake()
	{
		this.gameObject.GetComponent<ColorCorrectionCurves> ().saturation = 0.0f;
		final_value = 1.0F;
	}

	// Use this for initialization
	void Start () 
	{
		step = final_value / Level_System.time_before_start; // calculate the step to reach the final value base on level start time
        //reset_sat = true;
    }
	
	// Update is called once per frame
	void Update () 
	{
        //switch on and off the low pass filter
        if(active_LP)
        {
            GetComponent<AudioLowPassFilter>().enabled = true;
            GetComponent<AudioLowPassFilter>().cutoffFrequency = 2700;
            active_LP = false;
        }

        if (active_HP)
        {
            GetComponent<AudioLowPassFilter>().enabled = false;
            active_HP = false;
        }
        //--

        if (Saturation_Ctrl)
		{
			//Debug.Log ("change the Scen sat..");
			this.gameObject.GetComponent<ColorCorrectionCurves> ().saturation = Mathf.MoveTowards(start_value, final_value,step*Time.timeSinceLevelLoad);

			//reset saturation when it reach it final value
			if (this.gameObject.GetComponent<ColorCorrectionCurves> ().saturation >= final_value) 
			{
				Saturation_Ctrl = false;
				Board_Control.control = true;
			}
		}

		if (reset_sat) 
		{
			Saturation_Ctrl = true;
			reset_sat = false;
		}

	}
}
