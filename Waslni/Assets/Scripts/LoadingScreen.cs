﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LoadingScreen : MonoBehaviour {

    AsyncOperation ao; //save information about loading status
    public Text percet_loaded; // hold the value of the loading progress
    public Text qoute_text;
    public bool fake_LS; //choose between real and fake progress bar
    public float progress_increment = 10f; //save the amount of progress it show at a time
    public float progress_time = 1f; // time between each increment
    public bool fake_random_inc = false; //choose between random progress increment or pre selected
    public float min_inc = 1; // hold the minimum inc. value that a random function will generate
    public float max_inc = 30; // hold the maximum inc. value that a random function will generate
    public bool fake_random_time = false; //choose between random progress time or pre selected
    public float min_time = 0.5f; // hold the minimum time value that a random function will generate
    public float max_time = 3; // hold the maximum time value that a random function will generate
    float current_LS_percent;
    string[] qoutes = new string[51]; // change when you add more
    public float quote_time_change = 20;
    //used to initialize qoutes array values called before start
    void Awake()
    {
        qoutes[0] = "\"All great achievements require time.\" -Maya Angelou";
        qoutes[1] = "\"Patience is a conquering virtue.\" -Geoffrey Chaucer";
        qoutes[2] = "\"Growth is never by mere chance; it is the result of forces working together.\" -James Cash Penney";
        qoutes[3] = "\"He that can have patience can have what he will.\" -Benjamin Franklin";
        qoutes[4] = "\"Patience is bitter, but its fruit is sweet.\" -Aristotle";
        qoutes[5] = "\"Why is patience so important? Because it makes us pay attention.\" -Paulo Coelho";
        qoutes[6] = "\"I am extraordinarily patient, provided I get my own way in the end.\" -Margaret Thatcher";
        qoutes[7] = "\"The two most powerful warriors are patience and time.\" -Leo Tolstoy";
        qoutes[8] = "\"Persistence. Perfection. Patience. Power. Prioritize your passion. It keeps you sane.\" -Criss Jami";
        qoutes[9] = "\"If you do not believe you can do it then you have no chance at all.\" -Arsene Wenger";
        qoutes[10] = "\"Patience is the calm acceptance that things can happen in a different order than the one you have in mind.\" -David G. Allen";
        qoutes[11] = "\"And sure enough, even waiting will end...if you can just wait long enough.\" -William Faulkner";
        qoutes[12] = "\"Be yourself; everyone else is already taken.\" -Oscar Wilde";
        qoutes[13] = "\"I have not failed. I've just found 10,000 ways that won't work.\" -Thomas A. Edison";
        qoutes[14] = "\"It is never too late to be what you might have been.\" -George Eliot";
        qoutes[15] = "\"Life isn't about finding yourself. Life is about creating yourself.\" -George Bernard Shaw";
        qoutes[16] = "\"Do what you can, with what you have, where you are.\" - Theodore Roosevelt";
        qoutes[17] = "\"Success is not final, failure is not fatal: it is the courage to continue that counts.\" -Winston S. Churchill";
        qoutes[18] = "\"And, when you want something, all the universe conspires in helping you to achieve it.\" -Paulo Coelho";
        qoutes[19] = "\"It's the possibility of having a dream come true that makes life interesting.\" -Paulo Coelho";
        qoutes[20] = "\"Nothing is impossible, the word itself says 'I'm possible'!\" -Audrey Hepburn";
        qoutes[21] = "\"The most important thing is to enjoy your life—to be happy—it's all that matters.\" -Audrey Hepburn";
        qoutes[22] = "\"Happiness is not something ready made. It comes from your own actions.\" - Dalai Lama XIV";
        qoutes[23] = "\"Whatever you are, be a good one.\" -Abraham Lincoln";
        qoutes[24] = "\"Two wrongs don't make a right, but they make a good excuse.\" -Thomas Szasz";
        qoutes[25] = "\"Isn't it nice to think that tomorrow is a new day with no mistakes in it yet?\" -L.M. Montgomery";
        qoutes[26] = "\"Always do what you are afraid to do.\" -Ralph Waldo Emerson";
        qoutes[27] = "\"Our lives begin to end the day we become silent about things that matter.\" -Martin Luther King Jr.";
        qoutes[28] = "\"Who controls the past controls the future. Who controls the present controls the past.\" -George Orwell";
        qoutes[29] = "\"Pain is inevitable. Suffering is optional.\" -Haruki Murakami";
        qoutes[30] = "\"Fantasy is hardly an escape from reality. It's a way of understanding it.\" - Lloyd Alexander";
        qoutes[31] = "\"Turn your wounds into wisdom.\" -Oprah Winfrey";
        qoutes[32] = "\"It's amazing how a little tomorrow can make up for a whole lot of yesterday.\" -John Guare";
        qoutes[33] = "\"Yesterday is gone. Tomorrow has not yet come. We have only today. Let us begin.\" -Mother Teresa";
        qoutes[34] = "\"Real generosity towards the future lies in giving all to the present.\" -Albert Camus";
        qoutes[35] = "\"You only live once, but if you do it right, once is enough.\" -Mae West";
        qoutes[36] = "\"To live is the rarest thing in the world. Most people exist, that is all.\" -Oscar Wilde";
        qoutes[37] = "\"It is better to be hated for what you are than to be loved for what you are not.\" -André Gide";
        qoutes[38] = "\"Sometimes the questions are complicated and the answers are simple.\" -Dr. Seuss";
        qoutes[39] = "\"Some infinities are bigger than other infinities.\" -John Green";
        qoutes[40] = "\"Two things are infinite: the universe and human stupidity; and I'm not sure about the universe.\" -Albert Einstein";
        qoutes[41] = "\"Never put off till tomorrow what may be done day after tomorrow just as well.\" -Mark Twain";
        qoutes[42] = "\"I love deadlines. I love the whooshing noise they make as they go by.\" -Douglas Adams";
        qoutes[43] = "\"All you need is love. But a little chocolate now and then doesn't hurt.\" -Charles M. Schulz";
        qoutes[44] = "\"Some people never go crazy. What truly horrible lives they must lead.\" -Charles Bukowski";
        qoutes[45] = "\"If you ever find yourself in the wrong story, leave.\" - Mo Willems";
        qoutes[46] = "\"What's the good of living if you don't try a few things?\" -Charles M. Schulz";
        qoutes[47] = "\"So comes snow after fire, and even dragons have their endings.\" -J.R.R. Tolkien";
        qoutes[48] = "\"A man may die, nations may rise and fall, but an idea lives on. Ideas have endurance without death.\" -John F. Kennedy";
        qoutes[49] = "\"Sometimes there's not a better way. Sometimes there's only the hard way.\" -Mary E. Pearson";
        qoutes[50] = "\"Scars are not signs of weakness, they are signs of survival and endurance.\" -Rodney A. Winters";

    }

	// Use this for initialization
	void Start ()
    {
        //show a random qoute every X sec
        StartCoroutine(ChangeQuote());

        //check the progress bar mode Real or Fake 
        if (fake_LS)
        {
            StartCoroutine(FakeProgress());

        }
        else
        {
            StartCoroutine(RealProgress());
        }
	}
	
    //coroute funtion that pause and resume from selected point
    
    //Real loading progress bar function that increase the % and activate level when ready
    IEnumerator RealProgress()
    {
        //allow other function to be called such as update 
        yield return new WaitForSeconds(1);

        //Load screne in the BG and return a Async operation
        ao = SceneManager.LoadSceneAsync("MainMenu");

        //stop loaded level from activting 
        ao.allowSceneActivation = false;

        //loop while the loading is not done yet
        while(!ao.isDone)
        {
            //show progress in percent 
            percet_loaded.text = (ao.progress*100).ToString("00")+"%";

            //active the loaded level, 0.9 is the highest value that progress will reach if scene activation is not allowed
            if(ao.progress == 0.9f)
            {
                //set progress to 100%
                percet_loaded.text = "100%";

                //allow scene activation
                ao.allowSceneActivation = true;
            }

            //show current progress
            Debug.Log(ao.progress);
            yield return null;
        }
    }

    //Fale loading progress bar function that increase the % base on preseted mode and value
    //and activate level when ready
    IEnumerator FakeProgress()
    {
        //allow other function to be called such as update 
        yield return new WaitForSeconds(1);

        //Load screne in the BG and return a Async operation
        ao = SceneManager.LoadSceneAsync("MainMenu");

        //stop loaded level from activting 
        ao.allowSceneActivation = false;

        //loop while the loading is not done yet
        while (current_LS_percent <= 100)
        {

            //base on the mode the progress increment will be changed
            if (fake_random_inc)
            {
                progress_increment = Random.Range(min_inc, max_inc);
            }

            //base on the mode the progress time will be changed
            if (fake_random_time)
            {
                progress_time = Random.Range(min_time, max_time);
            }

            //increase the value of the progress
            current_LS_percent += progress_increment;

            //if the vlaue of progress is more that 100 reset to 100
            if (current_LS_percent > 100)
            {
                current_LS_percent = 100;
            }

            //show progress percent tp user
            percet_loaded.text = (current_LS_percent).ToString("00") + "%";

            //active the loaded level once progress is full
            if (current_LS_percent == 100)
            {
                //set progress to 100%
                percet_loaded.text = "100%";

                //allow scene activation
                ao.allowSceneActivation = true;
            }

            //show current progress
            //Debug.Log(ao.progress);
            yield return new WaitForSeconds(progress_time);
        }
    }

    IEnumerator ChangeQuote()
    {
        int count = 0;

        while (count < 100)
        {
            qoute_text.text = qoutes[Random.Range(0, qoutes.Length - 1)];
            count++;
            Debug.Log("QuoteChange#"+count);
            yield return new WaitForSecondsRealtime(quote_time_change);
            
        }

    }
    //-----------------------------------------------------------------------------------------------
}
