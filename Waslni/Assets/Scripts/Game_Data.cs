﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

// for save and load method
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

// used for google services 
using GooglePlayGames;
using UnityEngine.SocialPlatforms;

public class Game_Data : MonoBehaviour
{
    /* private Variables المتغيرات الخاصة */


    /* public Variables المتغيرات العامة */


    // static variables متغيرات ثابته
    public static Game_Data Data; // store an instance of this object 
	public static int number_of_LGs = 8;  // control the total number of level groups available
	public static int used_stars=0; // store the number of stars a player used
    public static int ads_stars = 0; // store number of stars player earn from videos
    public static int skip_point = 0; // number of point that used for skipping a level
	public static int number_of_lvl= 25 ; //  
    public static int number_of_items = 39 ;
    public static int number_of_achi = 7;
    public static int bought_stars = 0 ;
    public static bool death=false;
    public static bool first_time;
    public static bool mode_menu;
    public static string skin_name;

	public static bool[] opened_LG = new bool[number_of_LGs];
	public static bool[] opened_L = new bool[number_of_lvl];
	public static int[] lvl_star = new int[number_of_lvl];
    public static bool[] lvl_perfect = new bool[number_of_lvl]; // store if the leve have a perfect score or not 
    public static bool[] achi_unlocked = new bool[number_of_achi]; // achi unlocked indicator
    public static float[] best_time = new float[number_of_lvl]; // best time for all level in sec
    public static int[] level_rating = new int[number_of_lvl]; // store rating of all levels
    public static float[] level_timelimit = new float[number_of_lvl]; // store timelimit in sec for time trail mode 
    public static bool[] TT_done = new bool[number_of_lvl]; // store if the player already won this level in time trail mode
    public ScriptableObjectClass[] GameItems_m = new ScriptableObjectClass[number_of_items]; // it store a list of item used in the game by drop and drag in editor 
    public static ScriptableObjectClass[] GameItems = new ScriptableObjectClass[number_of_items]; // store that items in a static array for easie of access
    public static List<itemsownership> item_ownership = new List<itemsownership>(); // store the item name and if the player own it or not 
    public List<itemsownership> item_ownership_m = new List<itemsownership>();
    public bool resetIt = false;
    public static int death_count = 0;
    [SerializeField] public static Material player_mat;
    [SerializeField] public Material player_mat_m;

    // called before start also used for initialization
    void Awake()
    {

        player_mat = player_mat_m;
        //save data between scenes 
        if (Data == null)
        {
            DontDestroyOnLoad(gameObject); //keep the object even if scene changed
            Data = this; // assign this game object to the variable
        }
        else if (Data != this)  // destroy other game object if it had Data that is not this
        {
            Destroy(gameObject);
        }

        //temp potato till we got data or done with testing then we will adjust all manually
        for (int i = 0; i < number_of_lvl; i++)
        {
            level_timelimit[i] = 20+i*4;
        }

        for (int i = 0; i < number_of_items; i++)
        {
            GameItems[i] = GameItems_m[i];
        }

        if (resetIt || first_time)
        {
            Debug.Log("Data Reset Process");
            
            // reset the track mode to defualt
            PlayerPrefs.SetInt("Mode", 0);

            for (int i = 0; i < number_of_lvl; i++)
            {
                //Debug.Log (Game_Data.opened_LG [i] + "iteration is " + i);

                opened_L[i] = false;
                lvl_star[i] = 0;
                lvl_perfect[i] = false;
                best_time[i] = 0;
                level_rating[i] = 0;
                TT_done[i] = false;

            }

            for (int i = 0; i < number_of_achi; i++)
            {
                achi_unlocked[i] = false;
            }

            item_ownership.Clear(); // remove all the item in the list

            for (int i = 0; i < number_of_items; i++)
            {
                item_ownership.Add(new itemsownership(GameItems[i].ItemName, false)); // reset the value for all item to not owned
                PlayerPrefs.SetInt(GameItems[i].ItemName, 1);
                Debug.Log("reseting item ownership");
            }

            used_stars = 0;
            ads_stars = 0;
            skip_point = 0;
            bought_stars = 0;
            player_mat.SetColor("_Color", Color.white);
            skin_name = "Oraion";

            Save(); // save changes

            Debug.Log("inilized and saved data");
        }

        // copy the static item ownership list to non static list to be viewed in editor
        item_ownership_m = item_ownership;

        // authenticate user:
        Social.localUser.Authenticate((bool success) =>
        {
            // handle success or failure
            Debug.Log("user authenticated? " + success);
        });
    }

	//save player DATA to File  to access "Game_Data.Data.Save ();"
	public void Save()
	{
		BinaryFormatter BF = new BinaryFormatter (); // change the data to binary for ease of use
		FileStream file = File.Create(Application.persistentDataPath + "/PlayerData.dat"); // open a file and assaing a directory to it
		Player_Data data = new Player_Data ();  // create an instance of type player data 

		//loop to save all the level group player opened 
		for(int i =0 ; i < Game_Data.number_of_LGs; i++)
		{
			data.opened_LG [i] = opened_LG [i];
		}

		//loop to save all the level stars, opened levels, perfect levels, best times player have
		for(int i =0 ; i < number_of_lvl; i++)
		{
			data.lvl_star [i] = lvl_star [i];
			data.opened_L [i] = opened_L [i];
            data.lvl_perfect[i] = lvl_perfect[i];
            data.best_time[i] = best_time[i];
            data.level_rating[i] = level_rating[i];
            data.TT_done[i] = TT_done[i];
		}

        for (int i = 0; i < number_of_achi; i++)
        {
            data.achi_unlocked[i] = achi_unlocked[i];
        }

        data.item_ownership.Clear(); // remove all the item in the list

        for (int i = 0; i < number_of_items; i++)
        {
            data.item_ownership.Add(new itemsownership(GameItems[i].ItemName, item_ownership[i].item_owned));
        }

        data.used_stars = used_stars; // save the star player used
        data.ads_stars = ads_stars; // save number of stars earn throw video
        data.skip_point = skip_point; // save number of skip points
        data.bought_stars = bought_stars; // save number of bought stars
        data.mat_colorx = player_mat.color.r;
        data.mat_colory = player_mat.color.b;
        data.mat_colorz = player_mat.color.g;
        data.skin_name = skin_name;

        BF.Serialize (file,data); // it will take the data we want to save and serialize it to a file we assigned above
		file.Close(); //close the file after saveing
		Debug.Log("Data Saved");
	}


	//load player data to load simply access Game_Data.Data.Load();
	public void Load()
	{

        //check if PlayerData file is their if so it will load the data, if it is not their it will regenrate it
        if (File.Exists (Application.persistentDataPath + "/PlayerData.dat")) 
		{
			BinaryFormatter BF = new BinaryFormatter (); // change the data to binary for ease of use
			FileStream file = File.Open (Application.persistentDataPath + "/PlayerData.dat", FileMode.Open); // open a file and assaing a directory to it
			Player_Data data = (Player_Data)BF.Deserialize (file); // deserialize file and cast it as Player_Data file
			file.Close ();

			//loop to load all the level group player opened 
			for (int i = 0; i < number_of_LGs; i++) 
			{
				opened_LG [i] = data.opened_LG [i];
			}

            //loop to load all the  level stars, opened levels, perfect levels, best times player have
            for (int j=0 ;j < number_of_lvl;j++)
			{
				lvl_star [j] = data.lvl_star[j];
				opened_L [j] = data.opened_L [j] ;
                lvl_perfect[j] = data.lvl_perfect[j];
                best_time[j] = data.best_time[j];
                level_rating[j] = data.level_rating[j];
                TT_done[j] = data.TT_done[j];
			}

            for (int i = 0; i < number_of_achi; i++)
            {
                achi_unlocked[i] = data.achi_unlocked[i];
            }

            item_ownership.Clear(); // remove all the item in the list

            for (int i = 0; i < number_of_items; i++)
            {
                item_ownership.Add(new itemsownership(GameItems[i].ItemName, data.item_ownership[i].item_owned));
            }

            used_stars = data.used_stars; // load the numbe of stars player used
            ads_stars  = data.ads_stars;  // load number of stars earn throw video
            skip_point = data.skip_point; // load number of skip point 
            bought_stars = data.bought_stars; // load number bought stars
            player_mat.SetColor("_Color", new Color(data.mat_colorx, data.mat_colory, data.mat_colorz));

            for (int i = 0; i < GameItems.Length; i++)
            {
                if (data.skin_name == GameItems[i].ItemName)
                {
                    player_mat.SetTexture("_MainTex", Game_Data.GameItems[i].ItemMat.mainTexture);
                    player_mat.SetTexture("_BumpMap", Game_Data.GameItems[i].ItemMat.GetTexture("_BumpMap"));
                    player_mat.mainTextureScale = new Vector2(Game_Data.GameItems[i].ItemMat.mainTextureScale.x, Game_Data.GameItems[i].ItemMat.mainTextureScale.y);
                    player_mat.SetFloat("_Metallic", Game_Data.GameItems[i].ItemMat.GetFloat("_Metallic"));
                    player_mat.SetFloat("_Glossiness", Game_Data.GameItems[i].ItemMat.GetFloat("_Glossiness"));
                }
            }

            Debug.Log ("Data Loaded");
		} 
		else 
		{
			Debug.Log("Data Do Not Exist");

            // reset the track mode to defualt
            PlayerPrefs.SetInt("Mode", 0);

            for (int i = 0; i < number_of_lvl; i++)
            {
                //Debug.Log (Game_Data.opened_LG [i] + "iteration is " + i);
                opened_L[i] = false;
                lvl_star[i] = 0;
                lvl_perfect[i] = false;
                best_time[i] = 0;
                level_rating[i] = 0;
                TT_done[i] = false;
            }

            for (int i = 0; i < number_of_achi; i++)
            {
                achi_unlocked[i] = false;
            }

            for (int i = 0; i < number_of_items; i++)
            {
                item_ownership.Add(new itemsownership(GameItems[i].ItemName, false));
                PlayerPrefs.SetInt(GameItems[i].ItemName, 1);
            }

            used_stars = 0 ;
            ads_stars = 0 ;
            skip_point = 0 ;
            bought_stars = 0;
            player_mat.SetColor("_Color", Color.white);
            skin_name = "Oraion";

            Save(); // save changes

            Debug.Log("inilized and saved data");
        }
	}
}

[Serializable]
class Player_Data
{
	public bool[] opened_LG = new bool[Game_Data.number_of_LGs];
	public int used_stars ;
    public int ads_stars;
    public int skip_point;
    public float mat_colorx, mat_colory, mat_colorz;
    public string skin_name;
    public int[] lvl_star = new int[Game_Data.number_of_lvl];
	public bool[] opened_L = new bool[Game_Data.number_of_lvl];
    public bool[] lvl_perfect = new bool[Game_Data.number_of_lvl];
    public bool[] achi_unlocked = new bool[Game_Data.number_of_achi];
    public float[] best_time = new float[Game_Data.number_of_lvl];
    public int[] level_rating = new int[Game_Data.number_of_lvl];
    public bool[] TT_done = new bool[Game_Data.number_of_lvl];
    public int bought_stars;
    public List<itemsownership> item_ownership = new List<itemsownership>();

}

[Serializable]
public class itemsownership
{
    public string item_name;
    public bool item_owned;
    
    public itemsownership(string name,bool ownership)
    {
        item_name = name;
        item_owned = ownership;
    }
}
