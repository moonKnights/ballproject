﻿using UnityEngine;
using System.Collections;

public class Flooting_Gem : MonoBehaviour {
	public Rigidbody gem ;
	public float Torque_x = 1;
	public float Torque_y = 2;
	public float Torque_z= 3;
	public ForceMode force_mode = ForceMode.Acceleration;
    float time_scale_factor=1;
	// Use this for initialization
	void Start () 
	{
        gem = GetComponent<Rigidbody>();
    }
	
	// Update is called once per frame fixed update is not called when time is 0
	void FixedUpdate () 
	{
        gem.AddRelativeTorque(Torque_x,Torque_y , Torque_z , force_mode);

	}
    void Update()
    {
        if (Time.timeScale < 0.5)
        {
            time_scale_factor = (1 / 0.00001f) * 100;
            gem.GetComponent<Transform>().Rotate(Torque_x * time_scale_factor * Time.deltaTime, Torque_y * time_scale_factor * Time.deltaTime, Torque_z * time_scale_factor * Time.deltaTime);

        }
        else
            time_scale_factor = 1;

        //Debug.Log(time_scale_factor);
        

    }
}
