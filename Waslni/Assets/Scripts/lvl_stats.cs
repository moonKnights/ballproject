﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;// used for rawimage type
public class lvl_stats : MonoBehaviour 
{
	public GameObject Star_1;
	public GameObject Star_2;
	public GameObject Star_3;
    public Image RateIcon;
    public Text Level_number;
    public Text lvl_rating;
    public Image level_perfect;
	// Use this for initialization
	void Start () 
	{
        //Game_Data.lvl_star [5]=3;// debug only 

		//Debug.Log ("object name "+this.gameObject.name+" level "+int.Parse(this.gameObject.name));
		int level_star = Game_Data.lvl_star[ int.Parse(this.gameObject.name)-1]; 

        if(Game_Data.lvl_perfect [int.Parse(this.gameObject.name) - 1])
        {
            level_perfect.color = new Color(255, 255, 255, 255);
        }

        if(Game_Data.opened_L[int.Parse(this.gameObject.name) - 1])
        {
            RateIcon.color = new Color(255, 255, 255, 255);
            lvl_rating.color = new Color(255, 0, 69, 255);

            switch(Game_Data.level_rating[int.Parse(this.gameObject.name) - 1])
            {
                case 1: lvl_rating.text = "VE"; break;
                case 2: lvl_rating.text = "E"; break;
                case 3: lvl_rating.text = "N"; break;
                case 4: lvl_rating.text = "H"; break;
                case 5: lvl_rating.text = "VH"; break;
                default: lvl_rating.text = "NR"; break;
            }

        }

		switch(level_star)
		{
		case 1:
			Star_1.GetComponent<RawImage>().color = new Color(255,255,255,255);
			break;
		case 2:
			Star_1.GetComponent<RawImage>().color = new Color(255,255,255,255);
			Star_2.GetComponent<RawImage>().color = new Color(255,255,255,255);
			break;
		case 3:
			Star_1.GetComponent<RawImage>().color = new Color(255,255,255,255);
			Star_2.GetComponent<RawImage>().color = new Color(255,255,255,255);
			Star_3.GetComponent<RawImage>().color = new Color(255,255,255,255);
			break; 
		default:
			;break;
		}
	}
	
	// Update is called once per frame
	void Update () 
	{



	}
}
