﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "Item" , menuName = "Shop Item",order = 1)]
public class ScriptableObjectClass : ScriptableObject {

    public string ItemType = "PicItem,PicButItem,PicNumItem,MatItem";
    public string ItemName;
    public string ItemDescription;
    public bool ItemPriceP;
    public float ItemPrice;
    public bool ItemDiscount;
    public float ItemDiscountPercent;
    public bool ItemStatus;
    public Sprite ItemIcon;
    public float ItemStack;
    public AudioClip ItemListen;
    public Material ItemMat;
    public bool ItemRPriceP;
    public float ItemRPrice;
}
