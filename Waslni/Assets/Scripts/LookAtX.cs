﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtX : MonoBehaviour {


    public Transform target;
    public LayerMask playerlayer;
    public GameObject positionArrow;
	// Update is called once per frame
	void Update ()
    {
        transform.LookAt(target); // make the object forward direction always facing Player

        RaycastHit hit; // store the hitted object info.
        Ray RaytoPlayer = new Ray(transform.position, transform.forward); //start point and direction of cast

        Debug.DrawRay(transform.position, transform.forward*200);
        //cast ray and see if it hit player or not if not active arrow indicator
        if(Physics.Raycast(RaytoPlayer,out hit,playerlayer))
        {
            if(hit.collider.tag == "Player")
            {
                //Debug.Log("Hitting Player");
                positionArrow.SetActive(false);
            }
            else
            {
                positionArrow.SetActive(true);
                //Debug.Log(hit.collider.name);
            }

        }
	}
}
