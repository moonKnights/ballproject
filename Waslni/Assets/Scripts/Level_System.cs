﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using admob;

public class Level_System : MonoBehaviour {

	/* private Variables المتغيرات الخاصة */
	GameObject[] gems;
	bool enable = true;
    bool first_time = true;
	/* public Variables المتغيرات العامة */
	public GameObject cur_star_1;
	public GameObject cur_star_2;
	public GameObject cur_star_3;
	public GameObject end_level;
	public GameObject goals_menu;
    public GameObject mode_menu;
    public GameObject timeTrail_menu;

	public Text Score;
    public Text cur_Time;
    public Text best_time;
    public Text time_trail_CD;

    public int gems_number;
	public float time_before_start_M =2;

	// static variables متغيرات ثابته
	public static int current_stars = 0 ;
	public static int current_gems = 0 ;
	public static float time_before_start=2;
    public static float time_PlayerControl;
    float time_to_pick_amode=0;
    bool mode_selected= false;
    public static bool timetrail = false;
    public static float end_time;
    int scene_number_array;//get the scene number for array using 

    // Use this for initialization
    void Awake () 
	{
		//calculate how many gems in the scene
		time_before_start = time_before_start_M;
		//Debug.Log (time_before_start);
		gems = GameObject.FindGameObjectsWithTag ("Crystal");
		gems_number = gems.Length;

        #if UNITY_EDITOR
        Debug.Log("google ads dont work in Editor (initi)");
        #elif UNITY_ANDROID
        Admob.Instance().initAdmob("ca-app-pub-9760304122179353/9983394027","");
        Admob.Instance().setTesting(false);
        #endif
        
        //check to show player mode menu or not 
        if(Game_Data.mode_menu)
        {
            //stop time till player select a mode
            Time.timeScale = 0;

            //show mode menu to player 
            mode_menu.SetActive(true);

            //stop showing the menu till player go back to level selection 
            Game_Data.mode_menu = false;
        }
        else
        {
            //hide mode menu to player 
            mode_menu.SetActive(false);
            mode_selected = true;
        }
        scene_number_array = int.Parse(SceneManager.GetActiveScene().name) - 1;

    }

    void Start()
	{
        if (PauseMenu.muted)
        {
            Debug.Log("sound is muted");

            GameObject.Find("BGMusic").GetComponent<AudioSource>().mute = true;
        }
        else
        {
            Debug.Log("sound is not muted");

            //reset the audio source 
            GameObject.Find("BGMusic").GetComponent<AudioSource>().mute = false;
            //MusicCont.Sound_Source.enabled = true;

            //MusicCont.Sound_Source.mute = false;
        }

        current_gems = 0;
		current_stars = 0;

        #if UNITY_EDITOR
        Debug.Log("google ads dont work in Editor (Show)");
        #elif UNITY_ANDROID
        Admob.Instance().showBannerRelative(AdSize.Banner, AdPosition.TOP_CENTER, 0);
        #endif
        int scene_number1 = int.Parse(SceneManager.GetActiveScene().name) - 1;//get the scene number

        //display best and current time
        string Bminutes = Mathf.Floor(Game_Data.best_time[scene_number1] / 60).ToString("00");
        string Bseconds = ((Game_Data.best_time[scene_number1] % 60) * (100)).ToString("00:00");

        best_time.text = Bminutes + ":" + Bseconds;

        if (!goals_menu.activeSelf)
        {
            goals_menu.SetActive(true);
        }

        Debug.Log("Time trail is " + timetrail);



    }


	// Update is called once per frame
	void Update () 
	{

        // check if player have exceded the time limit for the level 
        if(Time.timeSinceLevelLoad > (time_before_start + time_to_pick_amode + Game_Data.level_timelimit[scene_number_array]) && timetrail)
        {
            Debug.Log("ur to slow ");
            //player die 
            retry.death();
        }

            if (goals_menu.activeSelf)
        {
            Board_Control.control = false;
        }



        if (Time.timeSinceLevelLoad>(time_before_start+time_to_pick_amode) && first_time && mode_selected) 
		{
            #if UNITY_EDITOR
            Debug.Log("google ads dont work in Editor (Remove)");
            #elif UNITY_ANDROID
            Admob.Instance().removeBanner();
            #endif
            goals_menu.SetActive (false);

            //if(SceneManager.GetActiveScene().name == "1")
            Board_Control.control = true;

            // get the time where player have control
            time_PlayerControl = Time.timeSinceLevelLoad;
            first_time = false;
        }

        if (Time.timeSinceLevelLoad > (time_before_start + time_to_pick_amode) && (Time.timeSinceLevelLoad - time_PlayerControl) > 0 && mode_selected)
        {
            string Cminutes = Mathf.Floor((Time.timeSinceLevelLoad - time_PlayerControl) / 60).ToString("00");
            string Cseconds = (((Time.timeSinceLevelLoad - time_PlayerControl) % 60)*(100)).ToString("00:00");

            cur_Time.text = Cminutes + ":" + Cseconds;

            // if in time trail mode
            if (timetrail)
            {
                //show time downcount
                timeTrail_menu.SetActive(true);

                //calculate time
                string CminutesTT = Mathf.Floor((Game_Data.level_timelimit[scene_number_array] - (Time.timeSinceLevelLoad - time_PlayerControl)) / 60).ToString("00");
                string CsecondsTT = (((Game_Data.level_timelimit[scene_number_array] - (Time.timeSinceLevelLoad - time_PlayerControl)) % 60) * (100)).ToString("00:00");

                //show reduced time 
                time_trail_CD.text = CminutesTT + ":" + CsecondsTT;
            }
            else
            {
                timeTrail_menu.SetActive(false);
            }

        }
        switch (current_stars)
		{
		case 1: cur_star_1.GetComponent<Image>().color = new Color(255,255,255,255);
			break;
		case 2: cur_star_1.GetComponent<Image>().color = new Color(255,255,255,255);
					 cur_star_2.GetComponent<Image>().color = new Color(255,255,255,255);
			break;
		case 3: cur_star_1.GetComponent<Image>().color = new Color(255,255,255,255);
			         cur_star_2.GetComponent<Image>().color = new Color(255,255,255,255);
			         cur_star_3.GetComponent<Image>().color = new Color(255,255,255,255);
			break;
		default:;
			break;
		}

		Score.text = current_gems+"/" + gems_number;

		if(current_gems == gems_number && enable)
		{
			current_stars++;
			end_level.SetActive (true);
			enable = false;
		}
			

	}

	public void Next_Level()
	{

        int scene_number = int.Parse (SceneManager.GetActiveScene ().name);//get the scene number

        float time_spent = end_time-(time_before_start + time_to_pick_amode);

        MyAnalytics.level_stat(scene_number, current_stars,Game_Data.death_count,time_spent, Game_Data.level_rating[scene_number-1]);
		scene_number++;
		Debug.Log ("next scene is level "+scene_number);
        Game_Data.death_count = 0;
        SceneManager.LoadScene ((scene_number).ToString());//load scene based on the name
	}

    //show be Level group
	public void Main_menu()
	{
        int scene_number = int.Parse(SceneManager.GetActiveScene().name);//get the scene number

        float time_spent = end_time - (time_before_start + time_to_pick_amode);

        MyAnalytics.level_stat(scene_number, current_stars, Game_Data.death_count, time_spent, Game_Data.level_rating[scene_number - 1]);

        Game_Data.death_count = 0;
        SceneManager.LoadScene ("LG1");

	}

	public void Replay()
	{
        int scene_number = int.Parse(SceneManager.GetActiveScene().name);//get the scene number

        float time_spent = end_time - (time_before_start + time_to_pick_amode);

        MyAnalytics.level_stat(scene_number, current_stars, Game_Data.death_count, time_spent, Game_Data.level_rating[scene_number - 1]);

        Game_Data.death_count = 0;
        Level_System.current_stars = 0;
		Level_System.current_gems = 0;
		SceneManager.LoadScene (SceneManager.GetActiveScene ().name);
	}

    public void AccelormeterToggle()
    {
        Board_Control.Enable_Acc = !Board_Control.Enable_Acc;

    }

    public void level_rating1()
    {
        int scene_number = int.Parse(SceneManager.GetActiveScene().name)-1;//get the scene number
        Game_Data.level_rating[scene_number]= 1;
        Game_Data.Data.Save();
    }

    public void level_rating2()
    {
        int scene_number = int.Parse(SceneManager.GetActiveScene().name) - 1;//get the scene number
        Game_Data.level_rating[scene_number] = 2;
        Game_Data.Data.Save();
    }

    public void level_rating3()
    {
        int scene_number = int.Parse(SceneManager.GetActiveScene().name) - 1;//get the scene number
        Game_Data.level_rating[scene_number] = 3;
        Game_Data.Data.Save();
    }

    public void level_rating4()
    {
        int scene_number = int.Parse(SceneManager.GetActiveScene().name) - 1;//get the scene number
        Game_Data.level_rating[scene_number] = 4;
        Game_Data.Data.Save();
    }

    public void level_rating5()
    {
        int scene_number = int.Parse(SceneManager.GetActiveScene().name) - 1;//get the scene number
        Game_Data.level_rating[scene_number] = 5;
        Game_Data.Data.Save();
    }

    public void Normal()
    {
        mode_selected = true;
        time_to_pick_amode = Time.timeSinceLevelLoad;
        mode_menu.SetActive(false);

        //show it in callenge 
        timeTrail_menu.SetActive(false);
    }

    public void TimeTrail()
    {
        mode_selected = true;
        time_to_pick_amode = Time.timeSinceLevelLoad;
        mode_menu.SetActive(false);
        timetrail = true;


    }

    public void TouchControl()
    {
        Board_Control.Enable_Acc = false;
    }

    public void AccelControl()
    {
        Board_Control.Enable_Acc = true;
    }
}
