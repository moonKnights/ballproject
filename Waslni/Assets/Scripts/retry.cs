using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class retry : MonoBehaviour {
	/* reload the scene when ball fall*/

	public GameObject deathsound;
    public bool death_m;
    public static GameObject deathsoundm;
    void Update()
    {
        death_m = Game_Data.death;
        deathsoundm = deathsound;
    }

    void OnTriggerEnter(Collider coll)
    {
        Debug.Log("its collder");

        //fix a bug where player die even if he finish the game since ball dont stop time dont freeze for some reason
        if (Finish_Level.End_Menu_m == null)
        {
            if (coll.gameObject.tag == "Player")
            {
                Game_Data.death = true; //set the death indicator to true if player died
                Instantiate(deathsound);
                Level_System.current_stars = 0;
                Level_System.current_gems = 0;
                Saturation_Control.reset_sat = true;
                Game_Data.death_count++;

                StartCoroutine(waitfor());
                SceneManager.LoadScene(SceneManager.GetActiveScene().name);


            }
        }
        else
        {
            if (coll.gameObject.tag == "Player" && !Finish_Level.End_Menu_m.activeSelf)
            {
                Game_Data.death = true; //set the death indicator to true if player died
                Instantiate(deathsound);
                Level_System.current_stars = 0;
                Level_System.current_gems = 0;
                Saturation_Control.reset_sat = true;
                Game_Data.death_count++;

                StartCoroutine(waitfor());
                SceneManager.LoadScene(SceneManager.GetActiveScene().name);


            }
        }
    }

    IEnumerator waitfor()
    {
        yield return new WaitForSeconds(1);
    }

    void OnCollisionEnter(Collision coll)
    {
        Debug.Log("death is here");

        //fix a bug where player die even if he finish the game since ball dont stop time dont freeze for some reason
        if (Finish_Level.End_Menu_m == null)
        {
            if (coll.gameObject.tag == "Player")
            {
                Game_Data.death = true; //set the death indicator to true if player died
                Instantiate(deathsound);
                Level_System.current_stars = 0;
                Level_System.current_gems = 0;
                Saturation_Control.reset_sat = true;
                Game_Data.death_count++;
                SceneManager.LoadScene(SceneManager.GetActiveScene().name);


            }
        }
    }

    public static void death()
    {
        Game_Data.death = true; //set the death indicator to true if player died
        Instantiate(deathsoundm);
        Level_System.current_stars = 0;
        Level_System.current_gems = 0;
        Saturation_Control.reset_sat = true;
        Game_Data.death_count++;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);

    }
}
