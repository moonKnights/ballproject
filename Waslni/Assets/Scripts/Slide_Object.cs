﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;// used for button type

public class Slide_Object : MonoBehaviour {

	/* private Variables المتغيرات الخاصة */


	/* public Variables المتغيرات العامة */
	public GameObject LGSlider ;
	public GameObject slide_left;
	public GameObject slide_right;

	public float Slide_value;

	//UI function used to slide the LG menu
	public void slide_r()
	{
		//if(debug_UI_s)
		//	Debug.Log ("enterd slide_r ");

		//used to Switch to level scene
		LGSlider.transform.localPosition = new Vector2 (LGSlider.transform.localPosition.x-Slide_value,LGSlider.transform.localPosition.y);
	}

	//UI function used to slide the LG menu
	public void slide_l()
	{
		//if(debug_UI_s)
		//	Debug.Log ("enterd slide_l ");

		//used to Switch to level scene
		LGSlider.transform.localPosition = new Vector2(LGSlider.transform.localPosition.x+Slide_value,LGSlider.transform.localPosition.y);
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		//check if the lG slider position and disable moveing in wrong direction
		if (LGSlider.transform.localPosition.x > -10) 
		{
			slide_left.GetComponent<Button> ().interactable = false;
		} 
		else 
		{
			slide_left.GetComponent<Button> ().interactable = true;
		}

		//check if the lG slider position and disable moveing in wrong direction
		if (LGSlider.transform.localPosition.x < -10) 
		{
			slide_right.GetComponent<Button> ().interactable = false;
		} 
		else 
		{
			slide_right.GetComponent<Button> ().interactable = true;
		}
	}
}
