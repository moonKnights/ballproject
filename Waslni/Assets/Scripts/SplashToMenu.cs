﻿using UnityEngine;
using UnityEngine.SceneManagement;
using GooglePlayGames;

public class SplashToMenu : MonoBehaviour {

    public float dealy=5;
    public AudioSource logotrack;
	// Use this for initialization
	void Start ()
    {
        //google services 
        PlayGamesPlatform.Activate();
        PlayGamesPlatform.DebugLogEnabled = true;

        logotrack.PlayDelayed(1.5f);
    }

    // Update is called once per frame
    void Update ()
    {

	    if(Time.time > dealy)
        {
            SceneManager.LoadScene("LoadingScreen");
        }
	}
}
