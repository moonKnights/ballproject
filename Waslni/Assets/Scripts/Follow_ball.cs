﻿using UnityEngine;
using System.Collections;

public class Follow_ball : MonoBehaviour {
	public Transform ball ;
    public float x_offset = 0;
    public float y_offset = 0;
    public float z_offset = 0;
    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		this.gameObject.transform.position = new Vector3 (ball.transform.position.x+ x_offset, ball.transform.position.y+ y_offset, ball.transform.position.z+ z_offset); 
	}
}
