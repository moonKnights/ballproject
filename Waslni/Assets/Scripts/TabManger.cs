﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TabManger : MonoBehaviour {

    public ScriptableObjectClass[] ItemsList = new ScriptableObjectClass[12];
    //public List<ItemList> ItemSelect = new List<ItemList>();

    public GameObject ShopItem;

    GameObject[] Active_items = new GameObject[100];

    // called once object is enabled
    void OnEnable()
    {
        /*for (int i = 0; i < ItemsList.Length; i++)
        {
            ItemSelect.Add(new ItemList(false, ItemsList[i].ItemName));
        }*/

        // create a temp game object to hold instanse of shop items
        GameObject temp_object;


        //loop throgh all the item in the list 
        for (int i = 0; i < ItemsList.Length; i++)
        {
            // instantiate a shop item
            temp_object = Instantiate(ShopItem);

            
            // change the parent of the instaniated shop item to the object that hold this script 
            // this object have an grid layout script that automaticlly arrange children items 
            temp_object.name = ItemsList[i].ItemName;
            temp_object.transform.SetParent(gameObject.transform);
            temp_object.transform.localScale = new Vector3(1, 1, 1);
            temp_object.transform.localPosition = new Vector3(0, 0, 0);



            // change the instance name,discription to that of the presetted item
            temp_object.GetComponent<ItemData>().ItemName.text = ItemsList[i].ItemName;
            temp_object.GetComponent<ItemData>().ItemDescription.text = ItemsList[i].ItemDescription;

            float discountP;

            if (ItemsList[i].ItemDiscountPercent > 0 && ItemsList[i].ItemDiscount)
            {
                discountP = 1 - ItemsList[i].ItemDiscountPercent / 100;
            }
            else
                discountP = 1;

            // check if the presetted item have star price, if yes then activate its button and assign price value to it, else deactivate the button
            if (ItemsList[i].ItemPriceP)
            {


                temp_object.GetComponent<ItemData>().ItemPriceP.SetActive(true);


                temp_object.GetComponent<ItemData>().ItemPrice.text = Mathf.Round(ItemsList[i].ItemPrice * discountP).ToString();

                //if preseted item dont have real price then center star price button // their are stange offset in the y value by 50 points 
                if (!ItemsList[i].ItemRPriceP)
                    temp_object.GetComponent<ItemData>().ItemPriceP.GetComponent<RectTransform>().transform.localPosition = new Vector3(0, -212.3f, 0);
            }
            else
            {
                temp_object.GetComponent<ItemData>().ItemPriceP.SetActive(false);
            }

            // check if the presetted item have real price, if yes then activate its button and assign price value to it, else deactivate the button
            if (ItemsList[i].ItemRPriceP)
            {
                temp_object.GetComponent<ItemData>().ItemRPriceP.SetActive(true);
                temp_object.GetComponent<ItemData>().ItemRPrice.text = (ItemsList[i].ItemRPrice * discountP).ToString();

                //if preseted item dont have star price then center real price button
                if (!ItemsList[i].ItemPriceP)
                    temp_object.GetComponent<ItemData>().ItemRPriceP.GetComponent<RectTransform>().transform.localPosition = new Vector3(0, -212.3f, 0);
            }
            else
            {
                temp_object.GetComponent<ItemData>().ItemRPriceP.SetActive(false);
            }

            // check if thier an dicount on the item if so then active dicount stamp and assign a dicount value to it, else deactive it
            if (ItemsList[i].ItemDiscount)
            {
                temp_object.GetComponent<ItemData>().ItemDiscount.SetActive(true);
                temp_object.GetComponent<ItemData>().ItemDiscountPercent.text = ItemsList[i].ItemDiscountPercent.ToString() + "%";
            }
            else
            {
                temp_object.GetComponent<ItemData>().ItemDiscount.SetActive(false);
            }

            // activate/deactivate new item stamp based on preseted setting
            if (ItemsList[i].ItemStatus)
                temp_object.GetComponent<ItemData>().ItemStatus.SetActive(true);
            else
                temp_object.GetComponent<ItemData>().ItemStatus.SetActive(false);

            switch (ItemsList[i].ItemType)
            {
                case "PicItem":
                    temp_object.GetComponent<ItemData>().ItemIcon.SetActive(true);
                    temp_object.GetComponent<ItemData>().Stacks.SetActive(false);
                    temp_object.GetComponent<ItemData>().ItemMat.SetActive(false);
                    temp_object.GetComponent<ItemData>().ItemListen.SetActive(false);

                    //setup the pic
                    temp_object.GetComponent<ItemData>().ItemIcon.GetComponent<Image>().sprite = ItemsList[i].ItemIcon;

                    // set the click button funtion to nonconsumable for ads
                    temp_object.GetComponent<ItemData>().ItemRPriceP.GetComponent<Button>().onClick.AddListener(delegate { temp_object.GetComponent<ItemData>().ItemRPriceP.GetComponent<Purchaser>().BuyNonConsumable(Purchaser.Product_No_Ads); });

                    break;

                case "PicButItem":
                    temp_object.GetComponent<ItemData>().ItemIcon.SetActive(true);
                    temp_object.GetComponent<ItemData>().Stacks.SetActive(false);
                    temp_object.GetComponent<ItemData>().ItemMat.SetActive(false);
                    temp_object.GetComponent<ItemData>().ItemListen.SetActive(true);

                    //setup the pic
                    temp_object.GetComponent<ItemData>().ItemIcon.GetComponent<Image>().sprite = ItemsList[i].ItemIcon;

                    break;

                case "PicNumItem":
                    temp_object.GetComponent<ItemData>().ItemIcon.SetActive(true);
                    temp_object.GetComponent<ItemData>().Stacks.SetActive(true);
                    temp_object.GetComponent<ItemData>().ItemMat.SetActive(false);
                    temp_object.GetComponent<ItemData>().ItemListen.SetActive(false);

                    //setup the pic and assign the stack number 
                    temp_object.GetComponent<ItemData>().ItemIcon.GetComponent<Image>().sprite = ItemsList[i].ItemIcon;
                    temp_object.GetComponent<ItemData>().ItemStack.text = "x" + ItemsList[i].ItemStack.ToString();

                    string temp_string = ItemsList[i].ItemStack.ToString();

                    // set the click button funtion to consumable
                    temp_object.GetComponent<ItemData>().ItemRPriceP.GetComponent<Button>().onClick.AddListener(delegate { temp_object.GetComponent<ItemData>().ItemRPriceP.GetComponent<Purchaser>().BuyConsumable(temp_string); });
                    break;
                case "MatItem":
                    temp_object.GetComponent<ItemData>().ItemIcon.SetActive(false);
                    temp_object.GetComponent<ItemData>().Stacks.SetActive(false);
                    temp_object.GetComponent<ItemData>().ItemMat.SetActive(true);
                    temp_object.GetComponent<ItemData>().ItemListen.SetActive(false);

                    //setup the material by copy color,texture,normalmap,tiling,metailc, smoothing values from presetted to instanse 
                    temp_object.GetComponent<ItemData>().ItemMat.GetComponent<MeshRenderer>().material.color = Shop_System.PlayerMat.color;
                    temp_object.GetComponent<ItemData>().ItemMat.GetComponent<MeshRenderer>().material.SetTexture("_MainTex", ItemsList[i].ItemMat.mainTexture);
                    temp_object.GetComponent<ItemData>().ItemMat.GetComponent<MeshRenderer>().material.SetTexture("_BumpMap", ItemsList[i].ItemMat.GetTexture("_BumpMap"));
                    temp_object.GetComponent<ItemData>().ItemMat.GetComponent<MeshRenderer>().material.mainTextureScale = new Vector2(ItemsList[i].ItemMat.mainTextureScale.x, ItemsList[i].ItemMat.mainTextureScale.y);
                    temp_object.GetComponent<ItemData>().ItemMat.GetComponent<MeshRenderer>().material.SetFloat("_Metallic", ItemsList[i].ItemMat.GetFloat("_Metallic"));
                    temp_object.GetComponent<ItemData>().ItemMat.GetComponent<MeshRenderer>().material.SetFloat("_Glossiness", ItemsList[i].ItemMat.GetFloat("_Glossiness"));
                    break;

                default:
                    Debug.Log("your type have a typo");
                    temp_object.GetComponent<ItemData>().ItemIcon.SetActive(false);
                    temp_object.GetComponent<ItemData>().Stacks.SetActive(false);
                    temp_object.GetComponent<ItemData>().ItemMat.SetActive(false);
                    temp_object.GetComponent<ItemData>().ItemListen.SetActive(false);
                    break;
            }

            bool item_not_owned = true;

            for (int j = 0; j < Game_Data.number_of_items; j++)
            {
                if (Game_Data.item_ownership[j].item_name == ItemsList[i].ItemName && Game_Data.item_ownership[j].item_owned)
                {
                    temp_object.GetComponent<ItemData>().ItemOwnership.SetActive(true);
                    temp_object.GetComponent<ItemData>().ItemPriceP.SetActive(false);
                    temp_object.GetComponent<ItemData>().ItemRPriceP.SetActive(false);
                    item_not_owned = false;
                }
            }

            if (item_not_owned)
            {
                temp_object.GetComponent<ItemData>().ItemOwnership.SetActive(false);
            }

            
            temp_object.GetComponent<ItemData>().CurItemData = ItemsList[i];

            //save the object that active in the tab for easy acess
            Active_items[i] = temp_object;


        }

        //Debug.Log(gameObject.name+" object enabled");

    }

    //called when bject is disabled 
    void OnDisable()
    {
        //Debug.Log(gameObject.name + " object disable");

        //list all the object who are child of theis gameobject
        for(int i=0; i < ItemsList.Length;i++)
        {
            //Debug.Log(Active_items[i].name);
            Destroy(Active_items[i]);
        }
    }
}
