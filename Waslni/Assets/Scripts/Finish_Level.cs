﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using admob;
using UnityEngine.SocialPlatforms;
using GooglePlayGames;

public class Finish_Level : MonoBehaviour 
{
	/* Go to the next Level when hit the portal crystal */
	public GameObject End_Menu;
    static public GameObject End_Menu_m;
	public GameObject cur_star_1;
	public GameObject cur_star_2;
	public GameObject cur_star_3;
    public GameObject NotBad;
    public GameObject Good;
    public GameObject Great;
    public GameObject Perfect;
    public GameObject DoneSFX;
    public GameObject AchiSFX;
    public GameObject[] achi_budge = new GameObject[7];
    public GameObject newBest;

    public Text BonceText;
    public Text cur_BTime;
    public Text best_time;
    public int Total_star_Before;
    public int Total_star_After;
    float start_time;
    float cur_best_time;
    int bonce_value=0;
    // called when object collide with another

    void Start()
    {
        End_Menu_m = End_Menu;
        bonce_value = 0;
        Total_star_Before = 0; // reset the value 
        for (int i = 0; i < Game_Data.number_of_lvl; i++)
        {
            Total_star_Before += Game_Data.lvl_star[i];
        }
    }

    void OnTriggerEnter(Collider coll)
	{
        Level_System.end_time = Time.timeSinceLevelLoad;
        #if UNITY_EDITOR
                Debug.Log("google ads dont work in Editor (Remove Finish level)");
        #elif UNITY_ANDROID
                    Admob.Instance().removeBanner();
        #endif




        if (coll.gameObject.tag == "Player")
		{
            Instantiate(DoneSFX);

            //reseting volume and pitch if player finish with effect on
            MusicCont.Sound_Source.volume = 0.3f;
            MusicCont.Sound_Source.pitch = 1;

            //load data 
            Game_Data.Data.Load();

            //loop throw all the item in the list 
            for (int i = 0; i < Game_Data.number_of_items; i++)
            {
                if (Game_Data.item_ownership[i].item_name == MusicCont.sceneTrack_m[int.Parse(SceneManager.GetActiveScene().name)].name)
                {
                    Game_Data.item_ownership[i].item_owned = true;
                    Game_Data.Data.Save();
                }
            }
                Board_Control.control = false; // stop player control
			Time.timeScale = 0; // stop time 

			int scene_number = int.Parse (SceneManager.GetActiveScene ().name);//get the scene number
			Game_Data.opened_L[scene_number] = true; // unlock the next level for player to play from

            if (Board_Control.Enable_Acc)
            {
                MyAnalytics.accelerometer_used(scene_number);
            }

            if(Level_System.timetrail)
            {
                MyAnalytics.TT_used(scene_number);
            }
            //save only if the current is higher than saved record
            scene_number -= 1; // for it be used in index 

			if(Level_System.current_stars>Game_Data.lvl_star[(scene_number)])
			{
				Game_Data.lvl_star[(scene_number)]=Level_System.current_stars; // save the number of start player earned
				//Debug.Log("Level Stars = "+Level_System.current_stars + " Record = "+ Game_Data.lvl_star[(scene_number)] +"scene "+ scene_number );
			}

            // record current best time 
            cur_best_time = Time.timeSinceLevelLoad-Level_System.time_PlayerControl;

            Debug.Log(Game_Data.best_time[scene_number] +" = "+ cur_best_time);
            //check if current best time is higher than existed one and replace it
            if (cur_best_time < Game_Data.best_time[scene_number] || Game_Data.best_time[scene_number] == 0)
            {
                Debug.Log("new best time");
                Game_Data.best_time[scene_number] = cur_best_time;
                newBest.SetActive(true);
            }
            else 
            {
                Debug.Log("not a new best time");
            }

            //display best and current time
            string Bminutes = Mathf.Floor(Game_Data.best_time[scene_number] / 60).ToString("00");
            string Bseconds = ((Game_Data.best_time[scene_number] % 60) * (100)).ToString("00:00");

            string CBminutes = Mathf.Floor(cur_best_time / 60).ToString("00");
            string CBseconds = ((cur_best_time % 60) * (100)).ToString("00:00");

            cur_BTime.text = CBminutes + ":" + CBseconds;
            best_time.text = Bminutes + ":" + Bseconds;

            End_Menu.SetActive (true);

            Debug.Log("current stars"+Level_System.current_stars);
            Debug.Log("player death" + Game_Data.death);

            switch (Level_System.current_stars)
			{
			case 1: cur_star_1.GetComponent<Image>().color = new Color(255,255,255,255);
                    NotBad.SetActive(true);
                    Debug.Log("Star1,NotBad");
                    break;
			case 2: cur_star_1.GetComponent<Image>().color = new Color(255,255,255,255);
				cur_star_2.GetComponent<Image>().color = new Color(255,255,255,255);
                    Good.SetActive(true);
                    Debug.Log("Star2,Good");
                    break;
			case 3: cur_star_1.GetComponent<Image>().color = new Color(255,255,255,255);
				cur_star_2.GetComponent<Image>().color = new Color(255,255,255,255);
				cur_star_3.GetComponent<Image>().color = new Color(255,255,255,255);
                    Debug.Log("Star3,Great or Perfect");
                    //if player died show great else show perfect and add a bonce star 
                    if (Game_Data.death)
                    {
                        Great.SetActive(true);
                    }
                    else
                    {
                        Perfect.SetActive(true);
                    }

                    if(!Game_Data.death && !Game_Data.lvl_perfect[scene_number])
                    {
                        Game_Data.lvl_perfect[scene_number] = true; // unlock the perfect state for this level
                        bonce_value++;
                    }
				break;
			default:Debug.Log("defult mode"+Level_System.current_stars);
				break;
			}

            //calculate the number of stars after finish level
            Total_star_After = 0; // reset the value 
            for (int i = 0; i < Game_Data.number_of_lvl; i++)
            {
                Total_star_After += Game_Data.lvl_star[i];
            }

            // post score to leaderboard ID 
            Social.ReportScore(Total_star_After,GPresources.leaderboard_level_stars, (bool success) => {
                // handle success or failure
            });

            if (Total_star_Before < 3 && Total_star_After >= 3)
            {
                bonce_value += 1;
                achi_budge[0].SetActive(true);
                Game_Data.achi_unlocked[0]= true;
                Debug.Log("Budge 1 3 star , before= " + Total_star_Before + " after =" + Total_star_After);
                Instantiate(AchiSFX);

                // unlock achievement 
                Social.ReportProgress(GPresources.achievement_3_stars, 100.0f, (bool success) => {
                    // handle success or failure
                });
            }

            if (Total_star_Before < 12 && Total_star_After >= 12)
            {
                bonce_value += 3;
                achi_budge[1].SetActive(true);
                Game_Data.achi_unlocked[1] = true;
                Debug.Log("Budge 2 12 star , before= " + Total_star_Before + " after =" + Total_star_After);
                Instantiate(AchiSFX);

                // unlock achievement 
                Social.ReportProgress(GPresources.achievement_12_stars, 100.0f, (bool success) => {
                    // handle success or failure
                });
            }

            if (Total_star_Before < 30 && Total_star_After >= 30)
            {
                bonce_value += 5;
                achi_budge[2].SetActive(true);
                Game_Data.achi_unlocked[2] = true;
                Debug.Log("Budge 3 30 star , before= " + Total_star_Before + " after =" + Total_star_After);
                Instantiate(AchiSFX);

                // unlock achievement 
                Social.ReportProgress(GPresources.achievement_30_stars, 100.0f, (bool success) => {
                    // handle success or failure
                });
            }

            if (Total_star_Before < 75 && Total_star_After >= 75)
            {
                bonce_value += 7;
                achi_budge[3].SetActive(true);
                Game_Data.achi_unlocked[3] = true;
                Debug.Log("Budge 4 75 star , before= " + Total_star_Before + " after =" + Total_star_After);
                Instantiate(AchiSFX);

                // unlock achievement 
                Social.ReportProgress(GPresources.achievement_75_stars, 100.0f, (bool success) => {
                    // handle success or failure
                });
            }

            if (Total_star_Before < 150 && Total_star_After >= 150)
            {
                bonce_value += 10;
                achi_budge[4].SetActive(true);
                Game_Data.achi_unlocked[4] = true;
                Debug.Log("Budge 5 150 star , before= " + Total_star_Before + " after =" + Total_star_After);
                Instantiate(AchiSFX);

                // unlock achievement 
                Social.ReportProgress(GPresources.achievement_150_stars, 100.0f, (bool success) => {
                    // handle success or failure
                });
            }

            if (Total_star_Before < 300 && Total_star_After >= 300)
            {
                bonce_value += 15;
                achi_budge[5].SetActive(true);
                Game_Data.achi_unlocked[5] = true;
                Debug.Log("Budge 6 300 star , before= " + Total_star_Before + " after =" + Total_star_After);
                Instantiate(AchiSFX);

                // unlock achievement 
                Social.ReportProgress(GPresources.achievement_300_stars, 100.0f, (bool success) => {
                    // handle success or failure
                });
            }

            if (Total_star_Before < 500 && Total_star_After >= 500)
            {
                bonce_value += 25;
                achi_budge[6].SetActive(true);
                Game_Data.achi_unlocked[6] = true;
                Debug.Log("Budge 7 500 star , before= " + Total_star_Before + " after =" + Total_star_After);
                Instantiate(AchiSFX);

                // unlock achievement 
                Social.ReportProgress(GPresources.achievement_500_stars, 100.0f, (bool success) => {
                    // handle success or failure
                });
            }

            int scene_numberTT = int.Parse(SceneManager.GetActiveScene().name)-1;//get the scene number

            //if time trail mode is on and its not  done for this level before
            if (Level_System.timetrail && !Game_Data.TT_done[scene_numberTT])
            {
                //indicate that time trail done for this level
                Game_Data.TT_done[scene_numberTT] = true;

                Debug.Log("player just earn 5 time master point");
                // increment time master achievement by 1 steps // achivment id need to be adde
                PlayGamesPlatform.Instance.IncrementAchievement(GPresources.achievement_time_master, 5, (bool success) => {/* handle success or failure */});


            }
            else
            {
                Debug.Log("player already finish that in TT mode or this normal mode");
            }

            Debug.Log(" before= " + Total_star_Before + " after =" + Total_star_After);

            BonceText.text = "x" + bonce_value;
            Game_Data.death = false; //reset death

            Game_Data.Data.Save(); //save all data 
        }
	
	}

}
