using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using admob;
using UnityEngine.UI;

public class PauseMenu : MonoBehaviour {

    public GameObject PausePanel;
    public GameObject clicksound;
    public GameObject collectionmenu;
    public GameObject optionmenu;
    float start_time, end_time;
    public static bool shop_active=false;
    public Text featured, skins, music;
    public GameObject featuredP, skinsP, musicP;
    public Text defualttext, allinone, selecttrack_m;
    public static string  selecttrack;
    public Text mute_text;
    public static bool muted=false;

    public void Replay()
    {
        retry.death();
    }

    public void featuredtab()
    {
        featured.color = new Color(255, 255, 255, 1);
        skins.color = new Color(255, 255, 255, 0.5f);
        music.color = new Color(255, 255, 255, 0.5f);
        // GameObject.ge

        featuredP.SetActive(true);
        skinsP.SetActive(false);
        musicP.SetActive(false);

    }

    public void skinstab()
    {
        featured.color = new Color(255, 255, 255, 0.5f);
        skins.color = new Color(255, 255, 255, 1);
        music.color = new Color(255, 255, 255, 0.5f);

        featuredP.SetActive(false);
        skinsP.SetActive(true);
        musicP.SetActive(false);

    }

    public void musictab()
    {
        featured.color = new Color(255, 255, 255, 0.5f);
        skins.color = new Color(255, 255, 255, 0.5f);
        music.color = new Color(255, 255, 255, 1);

        featuredP.SetActive(false);
        skinsP.SetActive(false);
        musicP.SetActive(true);

    }

    // Use this for initialization
    void Start ()
    {
        Time.timeScale = 1;

        if(PlayerPrefs.GetInt("Mode") == 0)
        {
            defualttext.color = new Color(255, 0, 90, 1);
            allinone.color = new Color(255, 255, 255, 0.5f);
        }
        else
        {
            defualttext.color = new Color(255, 255, 255, 0.5f);
            allinone.color = new Color(255, 0, 90, 1);
        }
    }
	
	// Update is called once per frame
	void Update ()
    {
        selecttrack_m.text= selecttrack;

        if(SceneManager.GetActiveScene().buildIndex<4)
        {
            Board_Control.control = true;
            Time.timeScale = 1f;
        }

	    if(Input.GetKeyDown(KeyCode.Escape)&& !shop_active && Board_Control.control)
        {
            Saturation_Control.active_LP = true;
            start_time = Time.realtimeSinceStartup;

            PausePanel.SetActive(true);
            Time.timeScale = 0.00001f;

            #if UNITY_EDITOR
                        Debug.Log("google ads dont work in Editor (Show (Pause))");
#elif UNITY_ANDROID
                        Admob.Instance().showBannerRelative(AdSize.Banner, AdPosition.TOP_CENTER, 0);
#endif

        }

        if (PlayerPrefs.GetInt("Mode") == 0)
        {
            defualttext.color = new Color(255, 0, 90, 1);
            allinone.color = new Color(255, 255, 255, 0.5f);
        }

        if (PlayerPrefs.GetInt("Mode") == 1)
        {
            defualttext.color = new Color(255, 255, 255, 0.5f);
            allinone.color = new Color(255, 0, 90, 1);
        }

        if (PausePanel.activeInHierarchy && SceneManager.GetActiveScene().buildIndex >= 4)
        Time.timeScale = 0.00001f;


    }

    public void Resume()
    {
        Saturation_Control.active_HP = true;

        #if UNITY_EDITOR
        Debug.Log("google ads dont work in Editor (Remove Resume)");
        #elif UNITY_ANDROID
        Admob.Instance().removeBanner();
        #endif

        Instantiate(clicksound);
        PausePanel.SetActive(false);
        Time.timeScale = 1;

        end_time = Time.realtimeSinceStartup;

        float time_spent = Mathf.Round(end_time - start_time);

        MyAnalytics.pausetime(time_spent);
        collectionmenu.SetActive(false);
        optionmenu.SetActive(false);
    }

    public void LG1()
    {

        end_time = Time.realtimeSinceStartup;
        float time_spent = Mathf.Round(end_time- start_time);

        #if UNITY_EDITOR
        Debug.Log("google ads dont work in Editor (Remove LGs)");
        #elif UNITY_ANDROID
        Admob.Instance().removeBanner();
        #endif

        MyAnalytics.pausetime(time_spent);
        collectionmenu.SetActive(false);
        optionmenu.SetActive(false);
        Instantiate(clicksound);
        SceneManager.LoadScene("LG1");
    }

    public void MainMenu()
    {
        end_time = Time.realtimeSinceStartup;
        float time_spent = Mathf.Round(end_time - start_time);

        #if UNITY_EDITOR
        Debug.Log("google ads dont work in Editor (Remove LGs)");
        #elif UNITY_ANDROID
                Admob.Instance().removeBanner();
        #endif
        collectionmenu.SetActive(false);
        optionmenu.SetActive(false);
        MyAnalytics.pausetime(time_spent);

        Instantiate(clicksound);
        SceneManager.LoadScene("MainMenu");
    }

    public void Quit()
    {
        end_time = Time.realtimeSinceStartup;
        float time_spent = Mathf.Round(end_time - start_time);

        MyAnalytics.pausetime(time_spent);

        Instantiate(clicksound);
        Application.Quit();
    }

    public void openCollectionmenu()
    {
        collectionmenu.SetActive(true);
        Instantiate(clicksound);
    }


    public void closeCollectionmenu()
    {
        collectionmenu.SetActive(false);
        Instantiate(clicksound);
    }

    public void open_options()
    {
        if (MusicCont.Sound_Source.mute)
        {
            mute_text.text = "unmute";
        }
        else
        {
            mute_text.text = "mute";
        }

        optionmenu.SetActive(true);
        Instantiate(clicksound);

    }

    public void close_options()
    {
        Instantiate(clicksound);
        optionmenu.SetActive(false);
    }

    public void TM_default()
    {
        Instantiate(clicksound);
        PlayerPrefs.SetInt("Mode", 0);
        defualttext.color = new Color(255,0, 90, 1);
        allinone.color = new Color(255, 255, 255, 0.5f);
        MusicCont.update_track = true;
    }

    public void TM_all_in_one()
    {
        Instantiate(clicksound);
        PlayerPrefs.SetInt("Mode", 1);
        defualttext.color = new Color(255, 255, 255, 0.5f);
        allinone.color = new Color(255, 0, 90, 1);
        MusicCont.update_track = true;
    }

    public void mute_sound()
    {
        //toggle sound mute on or off
        MusicCont.Sound_Source.mute = !MusicCont.Sound_Source.mute;

        Instantiate(clicksound);

        if (MusicCont.Sound_Source.mute)
        {
            mute_text.text = "unmute";
            muted = true;
        }
        else
        {
            mute_text.text = "mute";
            muted = false;
        }

    }
}
