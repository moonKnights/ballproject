﻿using UnityEngine;
using System.Collections;

public class debug_manager : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
		//enter debug mode for UI system
		if (Input.GetKeyDown (KeyCode.F1)) 
		{
			// state the start of debuging
			Debug.Log ("Start Debug UI_system...."); 

			//toggle debug mode
			UI_system.debug_UI_s = !UI_system.debug_UI_s;
		}
	}
}
