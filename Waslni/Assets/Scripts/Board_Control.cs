﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.CrossPlatformInput;

public class Board_Control : MonoBehaviour {
	public float speed = 100;
	public Transform target ;
	public Transform target2 ;
	public Transform target3 ;
	public Transform target4 ;
	public static bool control ;
	float step;

    float AccY, AccX,AxisH, AxisV,AxisH2, AxisV2;

    public static bool Enable_Acc = false;

	// Use this for initialization
	void Start () {
		control = false;
		//speed = 6000;
	}
    void Update()
    {
        //check if the player using accelmeter and stop screen from dim or turn off
        if(Enable_Acc)
        {
            //stop screen from turn off
            Screen.sleepTimeout = SleepTimeout.NeverSleep;
        }
        else
        {
            //reset screen setting to deafult
            Screen.sleepTimeout = SleepTimeout.SystemSetting;
        }
    }

	// FixedUpdate is called once per 0.02 sec
	void FixedUpdate () 
	{

		// The step size is equal to speed times frame time.
		step = speed * Time.deltaTime;

        //get the axis value of the joystick
        AxisH = CrossPlatformInputManager.GetAxis("Horizontal");
        AxisV = CrossPlatformInputManager.GetAxis("Vertical");

        AxisH2 = CrossPlatformInputManager.GetAxis("Horizontal2");
        AxisV2 = CrossPlatformInputManager.GetAxis("Vertical2");

        // Rotate our transform a step closer to the target's.
        if (control) 
		{
            //check if player is using accelemter mode
            if (Enable_Acc)
            {

                AccY = (float)System.Math.Round((double)(Input.acceleration.y), 2) * 100;
                AccX = (float)System.Math.Round((double)(Input.acceleration.x), 2) * 100;

                Debug.Log("X= " + Input.acceleration.x + " Y= " + Input.acceleration.y); // value from 0.3 to -0.3 for both, y give +up and -down, x +righ and -lift
                //Debug.Log("XAngel= " + AccX + " YAngel= " + AccY);

                if (AccX < -7) // left
                {
                    for (float i = 0f; i < (step * 0.01); i += 0.1f)
                    {
                        this.gameObject.transform.rotation = Quaternion.RotateTowards(this.gameObject.transform.rotation, target.rotation, i);
                    }
                }

                if (AccX > 7) // right
                {
                    for (float i = 0f; i < (step * 0.01); i += 0.1f)
                    {
                        this.gameObject.transform.rotation = Quaternion.RotateTowards(this.gameObject.transform.rotation, target2.rotation, i);
                    }
                }

                if (AccY < -40) // down
                {

                    for (float i = 0f; i < (step * 0.01); i += 0.1f)
                    {
                        this.gameObject.transform.rotation = Quaternion.RotateTowards(this.gameObject.transform.rotation, target3.rotation, i);
                    }
                }

                if (AccY > -35) // up
                {
                    for (float i = 0f; i < (step * 0.01); i += 0.1f)
                    {
                        this.gameObject.transform.rotation = Quaternion.RotateTowards(this.gameObject.transform.rotation, target4.rotation, i);
                    }
                }
            }

            //play using the new joystick controler 


            //Debug.Log ("Player have control");
            if (Input.GetKey (KeyCode.LeftArrow) || CrossPlatformInputManager.GetButton("Left") || Input.GetKey(KeyCode.A) || AxisH < 0 || AxisH2 < 0)
			{
				for (float i = 0f; i< (step * 0.01); i+= 0.1f) 
				{
					this.gameObject.transform.rotation = Quaternion.RotateTowards (this.gameObject.transform.rotation, target.rotation, i);
				}	
			}

			if (Input.GetKey (KeyCode.RightArrow)|| CrossPlatformInputManager.GetButton("Right") || Input.GetKey(KeyCode.D) || AxisH > 0 || AxisH2 > 0) 
			{
				for (float i = 0f; i< (step*0.01); i+= 0.1f) 
				{
					this.gameObject.transform.rotation = Quaternion.RotateTowards (this.gameObject.transform.rotation, target2.rotation, i);
				}
			}
			if (Input.GetKey (KeyCode.DownArrow)|| CrossPlatformInputManager.GetButton("Down") || Input.GetKey(KeyCode.S) || AxisV < 0 || AxisV2 < 0) 
			{
			
				for (float i = 0f; i< (step*0.01); i+= 0.1f) 
				{
					this.gameObject.transform.rotation = Quaternion.RotateTowards (this.gameObject.transform.rotation, target3.rotation, i);
				}
			}
		
			if (Input.GetKey (KeyCode.UpArrow)|| CrossPlatformInputManager.GetButton("Up") || Input.GetKey(KeyCode.W) || AxisV > 0 || AxisV2 > 0) 
			{
				for (float i = 0f; i< (step*0.01); i+= 0.1f) 
				{
					this.gameObject.transform.rotation = Quaternion.RotateTowards (this.gameObject.transform.rotation, target4.rotation, i);
				}
			}

            //Debug.Log("Xeuler= " + transform.rotation.eulerAngles.x + " Zeuler= " + transform.rotation.eulerAngles.z);
            
        }
    }

}
