﻿using UnityEngine;
using System.Collections;

public class SoundHit : MonoBehaviour {

    public GameObject wall_hit_sound;
    // Use this for initialization
    void OnCollisionEnter(Collision col)
    {
        //Debug.Log("collision enterd");
        if(col.gameObject.tag == "Player")
        {
            //Debug.Log("player is hitting a wall but its ");
            Instantiate(wall_hit_sound);
        }
    }
}
