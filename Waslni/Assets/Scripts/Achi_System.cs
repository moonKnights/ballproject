﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Achi_System : MonoBehaviour {

    public GameObject Achi_Panel;
    public GameObject Black_BG;
    public GameObject Button_fx;
    public GameObject[] Achi_Budge = new GameObject[7];
    public Text Achi_Count;

    int unlocked_Achi=0;
    
    public void Show_Achi()
    {
        Instantiate(Button_fx);
        Black_BG.SetActive(true);
        Achi_Panel.SetActive(true);
    }

    public void Back_Achi()
    {
        Instantiate(Button_fx);
        Black_BG.SetActive(false);
        Achi_Panel.SetActive(false);
    }
	// Use this for initialization
	void Start ()
    {
	    for(int i=0; i< Game_Data.number_of_achi; i++)
        {
            if(Game_Data.achi_unlocked[i])
            {
                Achi_Budge[i].GetComponent<Image>().color = Color.white;
                unlocked_Achi++;
            }
        }

        Achi_Count.text = unlocked_Achi + "/" + Game_Data.number_of_achi;

    }
	
	// Update is called once per frame
	void Update ()
    {
	
	}


}
