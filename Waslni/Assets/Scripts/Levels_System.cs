﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using admob;
public class Levels_System : MonoBehaviour {

	/* private Variables المتغيرات الخاصة */


	/* public Variables المتغيرات العامة */
	public GameObject[] Level = new GameObject[Game_Data.number_of_lvl];// store the level Available to ease access
    public static bool updateOpenedLvl = false;
    // static variables متغيرات ثابته

    void Awake()
    {
        #if UNITY_EDITOR
        Debug.Log("google ads dont work in Editor (initi)");
        #elif UNITY_ANDROID
        Admob.Instance().initAdmob("ca-app-pub-9760304122179353/9983394027","");
        Admob.Instance().setTesting(false);
        #endif
        
        //reseting the menu to shpw when player select a level
        Game_Data.mode_menu = true;
        Level_System.timetrail = false;

        //load player data
        Game_Data.Data.Load();
        Game_Data.opened_L[0] = true;			// always keep first level available
    }

    // Use this for initialization
    void Start () 
	{


		//unlock levels that player finished
		for (int i = 0; i < Game_Data.number_of_lvl; i++) 
		{
			if(Game_Data.opened_L[i])
			{
				//Debug.Log (i);
				Level[i].GetComponent<Button> ().interactable = true;
                Level[i].GetComponent<lvl_stats>().Level_number.color = Color.white; // increase text alpha for active levels
            }
		}

        //reset death indicator
        Game_Data.death= false;
	}
	
	// Update is called once per frame
	void Update ()
    {
	    if(updateOpenedLvl)
        {
            for (int i = 0; i < Game_Data.number_of_lvl; i++)
            {
                if (Game_Data.opened_L[i])
                {
                    //Debug.Log (i);
                    Level[i].GetComponent<Button>().interactable = true;
                    Level[i].GetComponent<lvl_stats>().Level_number.color = Color.white; // increase text alpha for active levels
                }
            }

            updateOpenedLvl = false;
        }
	}
}
