﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
public class DontDestroyObject : MonoBehaviour {

	// Use this for initialization
	void Awake () 
	{
		
		DontDestroyOnLoad (gameObject); //keep the object even if scene changed
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
