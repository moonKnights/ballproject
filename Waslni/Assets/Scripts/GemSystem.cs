using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class GemSystem : MonoBehaviour
{
    public int value = 1;
    public GameObject gemsound;
    public GameObject starEffect;
    public GameObject gemEffect;
    // called when object collide with another 
    void OnTriggerEnter(Collider coll)
    {
        if (coll.gameObject.tag == "Player")
        {

			if (this.gameObject.tag == "Star") 
			{
				Instantiate(gemsound);

                GameObject effecto;
                effecto = Instantiate(starEffect);
                effecto.transform.SetParent(gameObject.transform.parent);
                effecto.transform.localPosition = gameObject.transform.localPosition;
                Level_System.current_stars++;
				//Debug.Log (Level_System.current_stars);
			}

			if (this.gameObject.tag == "Crystal") 
			{
				Instantiate(gemsound);

                GameObject effecto;
                effecto = Instantiate(gemEffect);
                effecto.transform.SetParent(gameObject.transform.parent);
                effecto.transform.localPosition = gameObject.transform.localPosition;

                Level_System.current_gems++;
				//Debug.Log (Level_System.current_stars);
			}

            Destroy(this.gameObject);
        }
    }
}
