﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using admob;

public class MM_Options : MonoBehaviour {

    public GameObject Option_Menu;

	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.Escape))
        {
            Option_Menu.SetActive(true);

            #if UNITY_EDITOR
                Debug.Log("google ads dont work in Editor (Show (option))");
            #elif UNITY_ANDROID
                Admob.Instance().showBannerRelative(AdSize.Banner, AdPosition.TOP_CENTER, 0);
            #endif
            
        }
    }
}
