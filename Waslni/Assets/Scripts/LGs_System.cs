﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems; // to get the name of clicked button
using UnityEngine.UI;// used for button type
using admob;

// "/* */" this 2 simples are used to indicate Main Section 
// "//" this to indicate sub-section with-in a Main Section 
//array need to be under non-array in case we need to make it size define by a int variable

public class LGs_System : MonoBehaviour {

    /*
	Handle All the task need for Level groups scene from calculation of Available star to Unlock
	new groups and validation of  unlock process 
	*/

    /* private Variables المتغيرات الخاصة */
    int bonce_stars=0;

	/* public Variables المتغيرات العامة */
	public GameObject no_enough_stars; // to store the UI Element for player not haven enough star to unlock a level Group
	public GameObject black_BG; // UI element that cover the BG of pop-up UI with Transparent Black make other UI un-clickable
	public int number_of_LGs_mirror = 8;
	public bool reset_LGs = false;
	public int total_star_mirror;
    public GameObject button_sfx;

    public GameObject[] LGs= new GameObject[Game_Data.number_of_LGs];// store the level Group Available to ease access
	public GameObject[] LGs_UnlockUI= new GameObject[Game_Data.number_of_LGs];// store the level Group Available to ease access
	public bool[] opened_LG_mirror = new bool[8];
	public int[] required_star = {150,150,150,150,150,150,150};

	// static variables متغيرات ثابته 

	public static int total_stars_earned = 0;
	public static int available_stars;
	public static bool calc_stars ;

	public static int[] lvl_time = new int[Game_Data.number_of_lvl];

	// called before start also used for initialization
	void Awake () 
	{

        #if UNITY_EDITOR
        Debug.Log("google ads dont work in Editor (initi)");
#elif UNITY_ANDROID
        Admob.Instance().initAdmob("ca-app-pub-9760304122179353/9983394027","");
        Admob.Instance().setTesting(false);
#endif
    }

    void Start()
	{
        //load player data
        Game_Data.Data.Load();
        Game_Data.opened_LG[0] = true;          // always keep first level group available

        //unlock levels that player selected
        for (int i = 0; i < Game_Data.number_of_LGs; i++)
        {
            if (Game_Data.opened_LG[i])
            {
                //Debug.Log (i);
                LGs[i].GetComponent<Button>().interactable = true;
                LGs_UnlockUI[i].SetActive(false);
            }
        }

        //lvl_star [3] = 500; for debug only 
        calc_stars = true ;
	}

	// Update is called once per frame
	void Update ()
	{

		total_star_mirror = total_stars_earned;

		//calculate the total star earned if requested
		if(calc_stars)
		{
			total_stars_earned = 0; // reset the value 
			for (int i = 0; i < Game_Data.number_of_lvl; i++) 
			{
				total_stars_earned += Game_Data.lvl_star [i];
			}

            // add one bonce star for every perfect level
            for (int i = 0; i < Game_Data.number_of_lvl; i++)
            {
                if(Game_Data.lvl_perfect[i])
                bonce_stars++;
            }

            for (int i = 0; i < Game_Data.number_of_achi; i++)
            {
                if (Game_Data.achi_unlocked[i])
                {
                    switch (i)
                    {
                        case 0: bonce_stars += 1; break;
                        case 1: bonce_stars += 3; break;
                        case 2: bonce_stars += 5; break;
                        case 3: bonce_stars += 7; break;
                        case 4: bonce_stars += 10; break;
                        case 5: bonce_stars += 15; break;
                        case 6: bonce_stars += 25; break;
                    }
                }
            }

            total_stars_earned += bonce_stars;

            available_stars = total_stars_earned - Game_Data.used_stars;
			calc_stars = false;
			//Debug.Log ("Avaliable stars calculation is Done!");
		}

		//show the Level group that are available for player read-only 
		//also show the change in LGs array size
		for (int i = 0; i < Game_Data.number_of_LGs; i++) 
		{
			//Debug.Log (Game_Data.opened_LG [i] + "iteration is " + i);
			opened_LG_mirror[i] = Game_Data.opened_LG[i];

		}

		//make easy to change static variable from edior write-only
		if(number_of_LGs_mirror != 8)
		{
		Game_Data.number_of_LGs = number_of_LGs_mirror;
		Game_Data.opened_LG = new bool[Game_Data.number_of_LGs];
		opened_LG_mirror = new bool[Game_Data.number_of_LGs];
		}

		if(reset_LGs)
		{
			for (int i = 0; i < Game_Data.number_of_LGs; i++) 
			{
				//Debug.Log (Game_Data.opened_LG [i] + "iteration is " + i);
				Game_Data.opened_LG[i] = false;
			}
			Game_Data.used_stars = 0;
			Game_Data.Data.Save (); // save changes
		}
	}

	//UI function used to check if player have enough stars unlock a LG
	public void stars_check()
	{
        Instantiate(button_sfx);
        Debug.Log ("levelG name is "+int.Parse (EventSystem.current.currentSelectedGameObject.name));
		if (available_stars > required_star [int.Parse (EventSystem.current.currentSelectedGameObject.name)])
		{
			Game_Data.opened_LG [int.Parse (EventSystem.current.currentSelectedGameObject.name)] = true;
			Debug.Log (Game_Data.opened_LG [int.Parse (EventSystem.current.currentSelectedGameObject.name)]);
			Game_Data.used_stars += required_star [int.Parse (EventSystem.current.currentSelectedGameObject.name)];
			Debug.Log (LGs [int.Parse (EventSystem.current.currentSelectedGameObject.name)].name);
			LGs[int.Parse (EventSystem.current.currentSelectedGameObject.name)].GetComponent<Button> ().interactable = true;
			calc_stars = true;
			Game_Data.Data.Save ();//save change
			Destroy (EventSystem.current.currentSelectedGameObject);
		} 
		else 
		{
			//show no enough stars
			Debug.Log("Really niga get more stars");
			black_BG.SetActive (true);
			no_enough_stars.SetActive (true);
		}
	}

	//close no enough
	public void close_no_enough()
	{
        Instantiate(button_sfx);
        black_BG.SetActive (false);
		no_enough_stars.SetActive (false);
	}
}
