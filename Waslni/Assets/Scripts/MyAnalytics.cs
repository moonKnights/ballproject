﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;

public class MyAnalytics : MonoBehaviour {



    //get the time and trigger an event
    public static void shoptime(float stayed_time)
    {
        Analytics.CustomEvent("stayed_in_shop", new Dictionary<string, object> { {"time_in_sec",stayed_time}});
    }

    public static void purchase_item(string name,float cost,bool discount,float discount_p)
    {
        Analytics.CustomEvent("purchase_item_"+name, new Dictionary<string, object> { { "cost", cost }, { "discount", discount }, { "discount_percent", discount_p } });
    }

    public static void purchase_IAPitem( float cost)
    {
        Analytics.CustomEvent("purchase_stars", new Dictionary<string, object> { { "cost", cost } });
    }

    public static void skippted_alevel(string name)
    {
        Analytics.CustomEvent("skipped_levels", new Dictionary<string, object> { { "level_name", name } });
    }

    public static void accelerometer_used(int level_number)
    {
        Analytics.CustomEvent("Completed_with_Accelerometer", new Dictionary<string, object> { { "level_number", level_number } });
    }

    public static void level_stat(int level_number, int stars,int death,float time_spent,int rate)
    {
        Analytics.CustomEvent("level_"+level_number+"_state", new Dictionary<string, object> { { "level_number", level_number },{ "stars_collected" , stars }, { "death_count" , death }, { "time_spent" , time_spent }
        , { "level_rating" , rate }});
    }

    public static void pausetime(float timespent)
    {
        Analytics.CustomEvent("stayed_in_pause", new Dictionary<string, object> { { "time_in_sec", timespent } });
    }

    public static void TT_used(int level_number)
    {
        Analytics.CustomEvent("Completed_with_TimeTrial", new Dictionary<string, object> { { "level_number", level_number } });
    }
}
