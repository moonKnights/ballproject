﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkinAbility : MonoBehaviour
{
    float start_time_of_effectSW, start_time_of_effectFT, start_time_of_effectBG, start_time_of_effectSL;
    bool SW=false, FT=false, BG=false, SL=false;
    public float SW_duration = 10;
    public float FT_duration = 10;
    public float BG_duration = 10;
    public float SL_duration = 10;
    public float bigscale = 1.5f;
    public float smallscale = 0.5f;
    public PhysicMaterial[] physicEffect = new PhysicMaterial[3]; // def = 0 , slow = 1, fast ;
    public GameObject SlowSFX, FastSFX, SUpSFX, SDownSFX;
    float temp_pitch_value, temp_volume_value;
    bool SF_Effect_Appllied, BS_Effect_Appllied;

    // Update is called once per frame
    void Update()
    {
        if(SW && (Time.timeSinceLevelLoad - start_time_of_effectSW) > SW_duration)
        {
            gameObject.GetComponent<SphereCollider>().material = physicEffect[0];

            //reset the pitch when the effect finish
            MusicCont.Sound_Source.pitch = temp_pitch_value;

            SW = false;

            //indicate that the efect is finished
            SF_Effect_Appllied = false;
        }

        if (FT && (Time.timeSinceLevelLoad - start_time_of_effectFT) > FT_duration)
        {
            gameObject.GetComponent<SphereCollider>().material = physicEffect[0];

            //reset the pitch when the effect finish
            MusicCont.Sound_Source.pitch = temp_pitch_value;

            gameObject.GetComponent<Rigidbody>().drag = 3.37f;
            FT = false;

            //indicate that the efect is finished
            SF_Effect_Appllied = false;
        }

        if (BG && (Time.timeSinceLevelLoad - start_time_of_effectBG) > BG_duration)
        {
            gameObject.transform.localScale = new Vector3( 1 , 1 , 1 );

            //reset the volume when the effect finish
            MusicCont.Sound_Source.volume = temp_volume_value;

            BG = false;

            //indicate that the efect is finished
            BS_Effect_Appllied = false;
        }

        if (SL && (Time.timeSinceLevelLoad - start_time_of_effectSL) > SL_duration)
        {
            gameObject.transform.localScale = new Vector3( 1 , 1 , 1 );

            //reset the volume when the effect finish
            MusicCont.Sound_Source.volume = temp_volume_value;

            SL = false;

            //indicate that the efect is finished
            BS_Effect_Appllied = false;
        }
    }

    void OnTriggerEnter(Collider coll)
    {
        if( coll.tag == "Slow" )
        {
            Instantiate(SlowSFX);

            //change the pitch when the effect is applied

            //fix a bug if 2 same effect applied temp value is lost
            if(!SF_Effect_Appllied) 
                temp_pitch_value = MusicCont.Sound_Source.pitch;

            MusicCont.Sound_Source.pitch = 0.5f;

            //-------

            start_time_of_effectSW = Time.timeSinceLevelLoad;
            gameObject.GetComponent<SphereCollider>().material = physicEffect[1];
            SW = true;

            //indicate that slow or fast effect is applied
            SF_Effect_Appllied = true;

            Destroy(coll.gameObject);
        }

        if ( coll.tag == "Fast" )
        {
            Instantiate(FastSFX);

            //change the pitch when the effect is applied

            //fix a bug if 2 same effect applied temp value is lost
            if (!SF_Effect_Appllied)
                temp_pitch_value = MusicCont.Sound_Source.pitch;

            MusicCont.Sound_Source.pitch = 1.5f;

            //-----

            start_time_of_effectFT = Time.timeSinceLevelLoad;
            gameObject.GetComponent<SphereCollider>().material = physicEffect[2];
            gameObject.GetComponent<Rigidbody>().drag = 2;
            FT = true;

            //indicate that slow or fast effect is applied
            SF_Effect_Appllied = true;

            Destroy(coll.gameObject);
        }

        if ( coll.tag == "Big" )
        {
            Instantiate(SUpSFX);

            //change the volume when the effect is applied

            //fix a bug if 2 same effect applied temp value is lost
            if (!BS_Effect_Appllied)
                temp_volume_value = MusicCont.Sound_Source.volume;

            MusicCont.Sound_Source.volume = 1.0f;

            //-----

            start_time_of_effectBG = Time.timeSinceLevelLoad;
            gameObject.transform.localScale = new Vector3( 1 * bigscale, 1 * bigscale , 1 * bigscale );
            BG = true;

            //indicate that big or small effect is applied
            BS_Effect_Appllied = true;

            Destroy(coll.gameObject);
        }

        if ( coll.tag == "Small" )
        {
            Instantiate(SDownSFX);

            //change the volume when the effect is applied

            //fix a bug if 2 same effect applied temp value is lost
            if (!BS_Effect_Appllied)
                temp_volume_value = MusicCont.Sound_Source.volume;
            MusicCont.Sound_Source.volume = 0.1f;

            //-----

            start_time_of_effectSL = Time.timeSinceLevelLoad;
            gameObject.transform.localScale = new Vector3( 1 * smallscale , 1 * smallscale , 1 * smallscale );
            SL = true;

            //indicate that big or small effect is applied
            BS_Effect_Appllied = true;

            Destroy(coll.gameObject);
        }
    }
}
