﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Analytic_Activater : MonoBehaviour {

    // this code take is used to activate the analytic for each level
    // it take a button text value and use it as the level number to be add to analytic dashboard
    // the rest of the value are taken to be zeros
    // it require button element a text element and depend on MyAnalytics.C# Class.
    // a prefab is used so user only need to change the text element value with to a level number
    // button is set to call function Activate_Level()

    public Text levelnumber; // used to refreence a text element that hold a level number
	
	// Update is called once per frame
	public void Activate_Level() {


        //call a function in the analytic class that send level analytic
        MyAnalytics.level_stat(int.Parse(levelnumber.text), 0, 0, 0, 0);

        ///show a feedback massage of the level been activated
        Debug.Log("Activate Level " + int.Parse(levelnumber.text));
    }
}
