﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;

public class Admanger : MonoBehaviour {


    public GameObject CompleteStar;
    public GameObject CompleteSkip;
    public GameObject UltimateBlack;
    public GameObject Button_fx;
    // Use this for initialization
    void Awake()
    {
        Advertisement.Initialize("1269134", true);
	}
	
	// called once button clicked
	public void ShowAds()
    {
        Instantiate(Button_fx);
        ShowOptions options = new ShowOptions();
        options.resultCallback = AdCallBackHandler;

        while(!Advertisement.IsReady("rewardedVideo"))
        {
            Debug.Log("waiting");
        }
        
        Advertisement.Show("rewardedVideo", options);

    }

    void AdCallBackHandler(ShowResult result)
    {
        switch (result)
        {
            case ShowResult.Finished:
                Debug.Log("The ad was successfully shown.");
                //
                // YOUR CODE TO REWARD THE GAMER
                // Give coins etc.
                //
                
                if(Random.value <= 0.10)
                {
                    Game_Data.skip_point++;
                    SKipLevel.updateskipoint = true;

                    UltimateBlack.SetActive(true);// show black BG
                    CompleteSkip.SetActive(true); // show Earned done panel

                    Debug.Log("Player get a skip points 10% chance");
                }
                else
                {
                    Game_Data.ads_stars += 2; // player earn 2 stars
                    available_stars.calc_stars = true; // recalcuate number of start player have 

                    UltimateBlack.SetActive(true);// show black BG
                    CompleteStar.SetActive(true); // show Earned done panel

                    Debug.Log("Player get a 4 Stars 90% chance");
                }
                Game_Data.Data.Save(); // save data
                break;
            case ShowResult.Skipped:
                Debug.Log("The ad was skipped before reaching the end.");
                break;
            case ShowResult.Failed:
                Debug.LogError("The ad failed to be shown.");
                break;
        }
    }

    public void backfromVideo()
    {
        UltimateBlack.SetActive(false);
        CompleteSkip.SetActive(false);
        CompleteStar.SetActive(false);
    } 
}
