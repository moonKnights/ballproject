﻿using UnityEngine;
using UnityEngine.UI;

public class CollectionManger : MonoBehaviour {

    public GameObject CollectionItem;
    public string tabtype = "All,PicButItem,MatItem";
    // Use this for initialization
    void Start()
    {

        // create a temp game object to hold instanse of shop items
        GameObject temp_object;

        //loop throw all the item in the list 
        for (int i = 0; i < Game_Data.number_of_items; i++)
        {

            if (Game_Data.item_ownership[i].item_owned)
            {
                // instantiate a shop item
                temp_object = Instantiate(CollectionItem);

                if (Game_Data.GameItems[i].ItemType == "PicButItem" && tabtype == "PicButItem")
                {

                    // change the parent of the instaniated shop item to the object that hold this script 
                    // this object have an grid layout script that automaticlly arrange children items 
                    temp_object.name = Game_Data.item_ownership[i].item_name;
                    temp_object.transform.SetParent(gameObject.transform);
                    temp_object.transform.localScale = new Vector3(1, 1, 1);
                    temp_object.transform.localPosition = new Vector3(0, 0, 0);

                    // change the instance name,discription to that of the presetted item
                    temp_object.GetComponent<ItemData>().ItemName.text = Game_Data.item_ownership[i].item_name;

                    if (PlayerPrefs.GetInt(temp_object.GetComponent<ItemData>().ItemName.text) != 0)
                        temp_object.GetComponent<ItemData>().ItemStatus.SetActive(true);
                    else
                        temp_object.GetComponent<ItemData>().ItemStatus.SetActive(false);

                    switch (Game_Data.GameItems[i].ItemType)
                    {
                        case "PicButItem":
                            temp_object.GetComponent<ItemData>().ItemIcon.SetActive(true);
                            temp_object.GetComponent<ItemData>().ItemMat.SetActive(false);
                            temp_object.GetComponent<ItemData>().ItemListen.SetActive(true);

                            //setup the pic
                            temp_object.GetComponent<ItemData>().ItemIcon.GetComponent<Image>().sprite = Game_Data.GameItems[i].ItemIcon;
                            break;

                        default:
                            break;
                    }


                }

                if (Game_Data.GameItems[i].ItemType == "MatItem" && tabtype == "MatItem")
                {

                    // change the parent of the instaniated shop item to the object that hold this script 
                    // this object have an grid layout script that automaticlly arrange children items 
                    temp_object.name = Game_Data.item_ownership[i].item_name;
                    temp_object.transform.SetParent(gameObject.transform);
                    temp_object.transform.localScale = new Vector3(1, 1, 1);
                    temp_object.transform.localPosition = new Vector3(0, 0, 0);

                    // change the instance name,discription to that of the presetted item
                    temp_object.GetComponent<ItemData>().ItemName.text = Game_Data.item_ownership[i].item_name;

                    if (PlayerPrefs.GetInt(temp_object.GetComponent<ItemData>().ItemName.text) != 0)
                        temp_object.GetComponent<ItemData>().ItemStatus.SetActive(true);
                    else
                        temp_object.GetComponent<ItemData>().ItemStatus.SetActive(false);

                    switch (Game_Data.GameItems[i].ItemType)
                    {

                        case "MatItem":
                            temp_object.GetComponent<ItemData>().ItemIcon.SetActive(false);
                            temp_object.GetComponent<ItemData>().ItemMat.SetActive(true);
                            temp_object.GetComponent<ItemData>().ItemListen.SetActive(false);

                            //setup the material by copy color,texture,normalmap,tiling,metailc, smoothing values from presetted to instanse 
                            temp_object.GetComponent<ItemData>().ItemMat.GetComponent<MeshRenderer>().material.color = Shop_System.PlayerMat.color;
                            temp_object.GetComponent<ItemData>().ItemMat.GetComponent<MeshRenderer>().material.SetTexture("_MainTex", Game_Data.GameItems[i].ItemMat.mainTexture);
                            temp_object.GetComponent<ItemData>().ItemMat.GetComponent<MeshRenderer>().material.SetTexture("_BumpMap", Game_Data.GameItems[i].ItemMat.GetTexture("_BumpMap"));
                            temp_object.GetComponent<ItemData>().ItemMat.GetComponent<MeshRenderer>().material.mainTextureScale = new Vector2(Game_Data.GameItems[i].ItemMat.mainTextureScale.x, Game_Data.GameItems[i].ItemMat.mainTextureScale.y);
                            temp_object.GetComponent<ItemData>().ItemMat.GetComponent<MeshRenderer>().material.SetFloat("_Metallic", Game_Data.GameItems[i].ItemMat.GetFloat("_Metallic"));
                            temp_object.GetComponent<ItemData>().ItemMat.GetComponent<MeshRenderer>().material.SetFloat("_Glossiness", Game_Data.GameItems[i].ItemMat.GetFloat("_Glossiness"));
                            break;

                        default:
                            break;
                    }

                }

                if (tabtype == "All")
                {

                    // change the parent of the instaniated shop item to the object that hold this script 
                    // this object have an grid layout script that automaticlly arrange children items 
                    temp_object.name = Game_Data.item_ownership[i].item_name;
                    temp_object.transform.SetParent(gameObject.transform);
                    temp_object.transform.localScale = new Vector3(1, 1, 1);
                    temp_object.transform.localPosition = new Vector3(0, 0, 0);

                    // change the instance name,discription to that of the presetted item
                    temp_object.GetComponent<ItemData>().ItemName.text = Game_Data.item_ownership[i].item_name;

                    if (PlayerPrefs.GetInt(temp_object.GetComponent<ItemData>().ItemName.text) != 0)
                        temp_object.GetComponent<ItemData>().ItemStatus.SetActive(true);
                    else
                        temp_object.GetComponent<ItemData>().ItemStatus.SetActive(false);

                    switch (Game_Data.GameItems[i].ItemType)
                    {
                        case "PicButItem":
                            temp_object.GetComponent<ItemData>().ItemIcon.SetActive(true);
                            temp_object.GetComponent<ItemData>().ItemMat.SetActive(false);
                            temp_object.GetComponent<ItemData>().ItemListen.SetActive(true);

                            //setup the pic
                            temp_object.GetComponent<ItemData>().ItemIcon.GetComponent<Image>().sprite = Game_Data.GameItems[i].ItemIcon;
                            break;

                        case "MatItem":
                            temp_object.GetComponent<ItemData>().ItemIcon.SetActive(false);
                            temp_object.GetComponent<ItemData>().ItemMat.SetActive(true);
                            temp_object.GetComponent<ItemData>().ItemListen.SetActive(false);

                            //setup the material by copy color,texture,normalmap,tiling,metailc, smoothing values from presetted to instanse 
                            temp_object.GetComponent<ItemData>().ItemMat.GetComponent<MeshRenderer>().material.color = Shop_System.PlayerMat.color;
                            temp_object.GetComponent<ItemData>().ItemMat.GetComponent<MeshRenderer>().material.SetTexture("_MainTex", Game_Data.GameItems[i].ItemMat.mainTexture);
                            temp_object.GetComponent<ItemData>().ItemMat.GetComponent<MeshRenderer>().material.SetTexture("_BumpMap", Game_Data.GameItems[i].ItemMat.GetTexture("_BumpMap"));
                            temp_object.GetComponent<ItemData>().ItemMat.GetComponent<MeshRenderer>().material.mainTextureScale = new Vector2(Game_Data.GameItems[i].ItemMat.mainTextureScale.x, Game_Data.GameItems[i].ItemMat.mainTextureScale.y);
                            temp_object.GetComponent<ItemData>().ItemMat.GetComponent<MeshRenderer>().material.SetFloat("_Metallic", Game_Data.GameItems[i].ItemMat.GetFloat("_Metallic"));
                            temp_object.GetComponent<ItemData>().ItemMat.GetComponent<MeshRenderer>().material.SetFloat("_Glossiness", Game_Data.GameItems[i].ItemMat.GetFloat("_Glossiness"));
                            break;

                        default:
                            Debug.Log("your type have a typo");
                            temp_object.GetComponent<ItemData>().ItemIcon.SetActive(false);
                            temp_object.GetComponent<ItemData>().Stacks.SetActive(false);
                            temp_object.GetComponent<ItemData>().ItemMat.SetActive(false);
                            temp_object.GetComponent<ItemData>().ItemListen.SetActive(false);
                            break;
                    }


                }

                temp_object.GetComponent<ItemData>().CurItemData = Game_Data.GameItems[i];
            }
            
        }
    }
	// Update is called once per frame
	void Update ()
    {
		
	}


}
