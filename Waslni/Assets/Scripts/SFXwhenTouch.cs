﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SFXwhenTouch : MonoBehaviour
{

    public GameObject SFX;
    // called when object collide with another 
    void OnTriggerExit(Collider coll)
    {
        //if player finish the game and then fall the sound should not play 
        if (Finish_Level.End_Menu_m == null)
        {
            if (coll.gameObject.tag == "Player")
            {
                //MusicCont.Sound_Source.mute = true;
                Instantiate(SFX);
                Handheld.Vibrate(); // vibrate the device when player die
                StartCoroutine(waitfor());

            }
        }

    }
    void Update()
    {
       // Debug.Log(Time.timeScale);
    }

    IEnumerator waitfor()
    {
        Debug.Log("waiting");
        Time.timeScale = 0.2f;
        yield return new WaitForSecondsRealtime(1.2f);
        Debug.Log("done waiting");
        //reseting volume and pitch if player die with effect on
        MusicCont.Sound_Source.volume = 0.3f;
        MusicCont.Sound_Source.pitch = 1;

        Time.timeScale = 1;
    }
}
