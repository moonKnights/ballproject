﻿using UnityEngine;
using System.Collections;

public class Force_In_Dir : MonoBehaviour {

	public float Torque_x = 1;
	public float Torque_y = 2;
	public float Torque_z= 3;
	public ForceMode force_mode = ForceMode.Acceleration; 
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void OnTriggerEnter(Collider coll)
	{
		if (coll.gameObject.tag == "Player")
		{
			coll.GetComponent<Rigidbody>().AddForce(Torque_x,Torque_y,Torque_z,force_mode);

		}
	}
}
