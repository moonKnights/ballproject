﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SKipLevel : MonoBehaviour {

    public Text skippoints;
    public static bool updateskipoint = false;
    int leveltounlock;

	// Use this for initialization
	void Start ()
    {
        //update skip point counter
        skippoints.text = Game_Data.skip_point.ToString();
	}
	
	public void skipAlevel ()
    {
	    if(Game_Data.skip_point > 0)
        {
            //search for the level to unlock
            for (int i = 0; i < Game_Data.number_of_lvl; i++)
            {
                if (!Game_Data.opened_L[i])
                {
                    leveltounlock = i;
                    Debug.Log("Found level = " + (i + 1));
                    break;
                }
            }

            //unlock level 
            Game_Data.opened_L[leveltounlock] = true;

            MyAnalytics.skippted_alevel("level_" + leveltounlock);

            //reduce skip points
            Game_Data.skip_point--;

            //save
            Game_Data.Data.Save();

            //update counter
            skippoints.text = Game_Data.skip_point.ToString();

            //update levels UI
            Levels_System.updateOpenedLvl = true;
        }	
        else
        {
            Debug.Log("you dont have enough skiping points");
        }
	}

    void Update()
    {
        if(updateskipoint)
        {

            skippoints.text = Game_Data.skip_point.ToString();
            updateskipoint = false;
        }


    }
}
