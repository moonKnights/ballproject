﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement; // needed to use the new SceneManger
using UnityEngine.EventSystems; // to get the name of clicked button
using admob;

public class UI_system : MonoBehaviour {

	/* private Variables المتغيرات الخاصة */


	/* public Variables المتغيرات العامة */

	public GameObject black_BG; // UI element that cover the BG of pop-up UI with Transparent Black make other UI un-clickable
    public GameObject button_sfx;
    public GameObject show_credit;
    public GameObject options_menu;
    public bool reset_L;

    // static variables متغيرات ثابته
    public static bool debug_UI_s = false; // used for debug mode

    void Awake()
    {
        reset_L = false;

        #if UNITY_EDITOR
                Debug.Log("google ads dont work in Editor (initi)");
#elif UNITY_ANDROID
                Admob.Instance().initAdmob("ca-app-pub-9760304122179353/9983394027","");
                Admob.Instance().setTesting(false);
#endif
    }

    void Update()
    {
        if (reset_L)
        {
            for (int i = 0; i < Game_Data.number_of_lvl; i++)
            {
                //Debug.Log (Game_Data.opened_LG [i] + "iteration is " + i);
                Game_Data.opened_L[i] = false;
                Game_Data.lvl_star[i] = 0;
                Game_Data.lvl_perfect[i] = false;
            }

            for(int i =0; i<Game_Data.number_of_achi; i++)
            {
                Game_Data.achi_unlocked[i] = false;
            }

            Game_Data.used_stars = 0;

            Game_Data.Data.Save(); // save changes
            Debug.Log("lvls stars and unlock reset");
            reset_L = false;
        }
    }

    public void MainMenu()
    {
        Instantiate(button_sfx);

        if (debug_UI_s)
            Debug.Log("Back to Main Menu");

        SceneManager.LoadScene("MainMenu");
    }
	//UI function used to go to LGs 
	public void level_groups()
	{
        if (Game_Data.first_time)
        {
            Game_Data.Data.Save(); // save when player start the game so it avoid the problem of not find saved data 
            Game_Data.first_time = false;
            Debug.Log("this First time");
        }

        Instantiate(button_sfx);
        if (debug_UI_s)
			Debug.Log ("enterd level_groups Function Loading Scene...");
		//Game_Data.Data.Save (); // save the data so its always  do exist before any loading attemp
		//used to Switch to levels group scene
		SceneManager.LoadScene("LGs");
	}

    public void show_Option()
    {
        if (Game_Data.first_time)
        {
            Game_Data.Data.Save(); // save when player start the game so it avoid the problem of not find saved data 
            Game_Data.first_time = false;
            Debug.Log("this First time");
        }

        Game_Data.Data.Load();

        Instantiate(button_sfx);

        #if UNITY_EDITOR
                        Debug.Log("google ads dont work in Editor (Show (option))");
#elif UNITY_ANDROID
                        Admob.Instance().showBannerRelative(AdSize.Banner, AdPosition.TOP_CENTER, 0);
#endif

        options_menu.SetActive(true);
        
    }

    public void close_option()
    {
        Instantiate(button_sfx);
        Time.timeScale = 1;
        #if UNITY_EDITOR
                Debug.Log("google ads dont work in Editor (close options)");
        #elif UNITY_ANDROID
                Admob.Instance().removeBanner();
        #endif

        options_menu.SetActive(false);
    }
	//UI function used to go to selected level group
	public void load_LG()
	{
        Instantiate(button_sfx);
        if (debug_UI_s)
			Debug.Log ("enterd load_LG Function Loading Scene "+EventSystem.current.currentSelectedGameObject.name+".....");

		//used to Switch to levels group scene
		SceneManager.LoadScene(EventSystem.current.currentSelectedGameObject.name);
	}

	//UI function used to go to selected level
	public void load_Level()
	{
        Instantiate(button_sfx);
        if (debug_UI_s)
			Debug.Log ("enterd load_Level Function Loading Scene "+EventSystem.current.currentSelectedGameObject.name+".....");

		//used to Switch to level scene
		SceneManager.LoadScene(EventSystem.current.currentSelectedGameObject.name);
	}

    public void Credit()
    {
        Instantiate(button_sfx);
        show_credit.SetActive(true);
    }


    public void Creditback()
    {
        Instantiate(button_sfx);
        show_credit.SetActive(false);
    }
    //use to Exit game
    public void Exit()
	{
        Instantiate(button_sfx);
        Application.Quit ();
	}
}
