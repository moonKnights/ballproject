﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ItemData : MonoBehaviour {

    public Text ItemName;
    public Text ItemDescription;
    public GameObject ItemPriceP;
    public Text ItemPrice;
    public GameObject ItemDiscount;
    public Text ItemDiscountPercent;
    public GameObject ItemStatus;
    public GameObject ItemIcon;
    public GameObject Stacks;
    public Text ItemStack;
    public GameObject ItemListen;
    public GameObject ItemMat;
    public GameObject ItemOwnership;
    public GameObject ItemRPriceP;
    public Text ItemRPrice;
    public ScriptableObjectClass CurItemData;

    public void BuyStars()
    {
        float discountP;

        if (CurItemData.ItemDiscountPercent > 0 && CurItemData.ItemDiscount)
        {
            discountP = 1 - CurItemData.ItemDiscountPercent / 100;
        }
        else
            discountP = 1;

        //check if the player have enough stars or not if not till him that he cant buy it if he have user get it and reduce value he have
        if (available_stars.available_star < Mathf.Round(CurItemData.ItemPrice*discountP))
        {
            Shop_System.NoEnoughStars = true;
        }
        else
        {
            //reduce the value 
            Game_Data.used_stars += int.Parse( Mathf.Round(CurItemData.ItemPrice*discountP).ToString());
            available_stars.calc_stars = true;

            //send an costume event that this item is bought
            MyAnalytics.purchase_item(CurItemData.name, CurItemData.ItemPrice, CurItemData.ItemDiscount, CurItemData.ItemDiscountPercent);

            switch (CurItemData.ItemType)
            {
                case "PicItem":
                    Shop_System.ColorPick = true;
                    break;

                case "PicButItem":
                    Debug.Log("this a music now player should have it on its play list");

                    for (int i = 0; i < Game_Data.number_of_items; i++)
                    {
                        if(Game_Data.item_ownership[i].item_name == CurItemData.ItemName)
                        {
                            Game_Data.item_ownership[i].item_owned = true;
                            Game_Data.Data.Save();

                            ItemOwnership.SetActive(true);
                            ItemPriceP.SetActive(false);
                            ItemRPriceP.SetActive(false);
                        }
                    }
                        break;

                case "MatItem":

                    //setup the material by copy color,texture,normalmap,tiling,metailc, smoothing values from presetted to instanse 
                    //Shop_System.PlayerMat.color = CurItemData.ItemMat.color;
                    Shop_System.PlayerMat.SetTexture("_MainTex", CurItemData.ItemMat.mainTexture);
                    Shop_System.PlayerMat.SetTexture("_BumpMap", CurItemData.ItemMat.GetTexture("_BumpMap"));
                    Shop_System.PlayerMat.mainTextureScale = new Vector2(CurItemData.ItemMat.mainTextureScale.x, CurItemData.ItemMat.mainTextureScale.y);
                    Shop_System.PlayerMat.SetFloat("_Metallic", CurItemData.ItemMat.GetFloat("_Metallic"));
                    Shop_System.PlayerMat.SetFloat("_Glossiness", CurItemData.ItemMat.GetFloat("_Glossiness"));

                    for (int i = 0; i < Game_Data.number_of_items; i++)
                    {
                        if (Game_Data.item_ownership[i].item_name == CurItemData.ItemName)
                        {
                            Game_Data.item_ownership[i].item_owned = true;
                            Game_Data.skin_name = Game_Data.item_ownership[i].item_name;
                            Game_Data.Data.Save();
                            ItemOwnership.SetActive(true);
                            ItemPriceP.SetActive(false);
                            ItemRPriceP.SetActive(false);
                        }
                    }

                    break;
                default:
                    Debug.Log("your type have a typo bro this an item data");

                    break;
            }
        }

    }


    public void Listen()
    {
        MusicCont.stop_music = true;
        MusicCont.lasten_track = CurItemData.ItemListen;
        MusicCont.StartAfter = CurItemData.ItemListen.length + 1;
    }

    void Update()
    {
        ItemMat.GetComponent<MeshRenderer>().material.color = Shop_System.PlayerMat.color;
    }

    public void Seen()
    {
        //Debug.Log(ItemName.text + " exist? " + PlayerPrefs.HasKey(ItemName.text) + " value= " + PlayerPrefs.GetInt(ItemName.text));

        if (PlayerPrefs.GetInt(ItemName.text) != 0)
        {
            PlayerPrefs.SetInt(ItemName.text, 0);
            //Debug.Log(ItemName.text + " market as seen");
            ItemStatus.SetActive(false);
        }
    }

    public void applyitem()
    {
        switch (CurItemData.ItemType)
        {

            case "PicButItem":
                Debug.Log("apply music");
                //track system 
                MusicCont.selected_track = CurItemData.ItemListen;
                //change the mode to all for 1
                PlayerPrefs.SetInt("Mode", 1);
                //update track system
                MusicCont.update_track = true;
                break;

            case "MatItem":

                //setup the material by copy color,texture,normalmap,tiling,metailc, smoothing values from presetted to instanse 
                //Shop_System.PlayerMat.color = CurItemData.ItemMat.color;
                Shop_System.PlayerMat.SetTexture("_MainTex", CurItemData.ItemMat.mainTexture);
                Shop_System.PlayerMat.SetTexture("_BumpMap", CurItemData.ItemMat.GetTexture("_BumpMap"));
                Shop_System.PlayerMat.mainTextureScale = new Vector2(CurItemData.ItemMat.mainTextureScale.x, CurItemData.ItemMat.mainTextureScale.y);
                Shop_System.PlayerMat.SetFloat("_Metallic", CurItemData.ItemMat.GetFloat("_Metallic"));
                Shop_System.PlayerMat.SetFloat("_Glossiness", CurItemData.ItemMat.GetFloat("_Glossiness"));
                Game_Data.skin_name = CurItemData.ItemName;
                Game_Data.Data.Save();
                break;
            default:
                Debug.Log("your type have a typo, bro this an item data");

                break;
        }
    }
}
