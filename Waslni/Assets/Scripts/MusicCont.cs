﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MusicCont : MonoBehaviour {

    // static variables متغيرات ثابته
    public static MusicCont MusicBG;
    public int starting_level = 4;
    public static bool stop_music;
    public static float StartAfter;
    float StartTime;
    public AudioClip[] sceneTrack = new AudioClip[Game_Data.number_of_lvl+1];
    public static AudioClip[] sceneTrack_m = new AudioClip[Game_Data.number_of_lvl + 1];
    public static AudioClip selected_track,lasten_track;
    public static bool update_track=false;
    AudioClip temp_track;
    public static bool low_volume=false;
    public static AudioSource Sound_Source;
    // called before start also used for initialization
    void Awake()
    {
        Sound_Source = GetComponent<AudioSource>();

        //copy scene track library to static variable to be easily access
        sceneTrack_m = sceneTrack;

        update_track = true;

        //play music between scenes 
        if (MusicBG == null)
        {
            DontDestroyOnLoad(gameObject); //keep the object even if scene changed
            MusicBG = this; // assign this game object to the variable
        }
        else if (MusicBG != this)  // destroy other game object if it had Music that is not this
        {
            Destroy(gameObject);
        }

    }


    void Update()
    {
        if (Sound_Source == null)
        {
            Sound_Source = GetComponent<AudioSource>();
        }

        if (update_track)
        {
            //Debug.Log("update entered");

            //check if the mode is deafult or not
            if (PlayerPrefs.GetInt("Mode") == 0)
            {
                //check if the user in menus scene 
                if (SceneManager.GetActiveScene().buildIndex < starting_level)
                {
                    //check if their no track playing or not
                    if (GetComponent<AudioSource>().clip == null)
                    {
                        //assign first clip "N" to the audio source
                        GetComponent<AudioSource>().clip = sceneTrack[0];

                        //change the name of selected track
                        PauseMenu.selecttrack = GetComponent<AudioSource>().clip.name;

                        //reset the audio source 
                        GetComponent<AudioSource>().enabled = false;
                        GetComponent<AudioSource>().enabled = true;
                    }
                    else
                    {
                        // check if the truck that is playing is not "N", and reset to "N"
                        if (GetComponent<AudioSource>().clip.name != sceneTrack[0].name)
                        {
                            //assign first clip "N" to the audio source
                            GetComponent<AudioSource>().clip = sceneTrack[0];

                            //change the name of selected track
                            PauseMenu.selecttrack = GetComponent<AudioSource>().clip.name;

                            //reset the audio source 
                            GetComponent<AudioSource>().enabled = false;
                            GetComponent<AudioSource>().enabled = true;
                        }
                    }

                }

                //check if the user is in a level not a menu scene make sure that truck dont repeat if player die
                if (SceneManager.GetActiveScene().buildIndex >= starting_level && Game_Data.death_count == 0)
                {
                    //assign level track to the audio source
                    GetComponent<AudioSource>().clip = sceneTrack[int.Parse(SceneManager.GetActiveScene().name)];

                    //change the name of selected track
                    PauseMenu.selecttrack = GetComponent<AudioSource>().clip.name;

                    //reset the audio source 
                    GetComponent<AudioSource>().enabled = false;
                    GetComponent<AudioSource>().enabled = true;
                }
            }
            else // if it one for all mode 
            {
                //check if their any track select to play it or play "N" instead
                if (selected_track == null)
                {
                    //assign "N" to the audio source
                    GetComponent<AudioSource>().clip = sceneTrack[0];

                    //change the name of selected track
                    PauseMenu.selecttrack = GetComponent<AudioSource>().clip.name;

                    //reset the audio source 
                    GetComponent<AudioSource>().enabled = false;
                    GetComponent<AudioSource>().enabled = true;
                }
                else
                {
                    //check if it the same track to not replay it to avoid cutting sound when restart
                    if (GetComponent<AudioSource>().clip.name != selected_track.name)
                    {
                        //assign "N" to the audio source
                        GetComponent<AudioSource>().clip = selected_track;

                        //change the name of selected track
                        PauseMenu.selecttrack = GetComponent<AudioSource>().clip.name;

                        //reset the audio source 
                        GetComponent<AudioSource>().enabled = false;
                        GetComponent<AudioSource>().enabled = true;
                    }
                }
            }

            update_track = false;
        }

        if (stop_music)
        {
            // save the track is playing now
            temp_track = GetComponent<AudioSource>().clip;
            // play the lesten track
            GetComponent<AudioSource>().clip = lasten_track;

            //reset the audio source 
            GetComponent<AudioSource>().enabled = false;
            GetComponent<AudioSource>().enabled = true;

            //play track only once
            GetComponent<AudioSource>().loop = false;

            //set start time
            StartTime = Time.timeSinceLevelLoad;

            //change the name of selected track
            PauseMenu.selecttrack = GetComponent<AudioSource>().clip.name;
            
            Debug.Log("started lasten");
            stop_music = false;
        }

        /* debug the lestan* system 
        if (lasten_track != null)
            Debug.Log(lasten_track.name+" " + StartAfter + " " + (Time.timeSinceLevelLoad - StartTime));
        else
            Debug.Log("null " + StartAfter + " " + (Time.timeSinceLevelLoad - StartTime));
        */

        // check if the track is finished and reset the old one
        if(Time.timeScale > 0.5)
        {
            if ((Time.timeSinceLevelLoad - StartTime) > (StartAfter) && lasten_track != null)
            {
                Debug.Log("here we are finished");
                lasten_track = null;
                //play the track that was before start lasten
                GetComponent<AudioSource>().clip = temp_track;

                //reset the audio source 
                GetComponent<AudioSource>().enabled = false;
                GetComponent<AudioSource>().enabled = true;

                //enable track looping
                GetComponent<AudioSource>().loop = true;

                //change the name of selected track
                PauseMenu.selecttrack = GetComponent<AudioSource>().clip.name;
            }
        }
        else
        {
            if ((Time.timeSinceLevelLoad - StartTime) > (StartAfter * (0.00001f)) && lasten_track != null)
            {
                Debug.Log("here we are finished");
                lasten_track = null;
                //play the track that was before start lasten
                GetComponent<AudioSource>().clip = temp_track;

                //reset the audio source 
                GetComponent<AudioSource>().enabled = false;
                GetComponent<AudioSource>().enabled = true;

                //enable track looping
                GetComponent<AudioSource>().loop = true;

                //change the name of selected track
                PauseMenu.selecttrack = GetComponent<AudioSource>().clip.name;
            }
        }
        
    }
}
