using UnityEngine;
using System.Collections;

public class Contact : MonoBehaviour {

	string Message= "Playing #OraionTheRollingBall @4knightsgames https://goo.gl/z2AtzD";
    public GameObject button_sfx;
	void Start()
	{

	}
	public void Web()
    {
        Instantiate(button_sfx);
        Application.OpenURL("http://4knights.net/contact");
    }

    public void FB()
    {
        Instantiate(button_sfx);
        Application.OpenURL("https://www.facebook.com/4knightsgames/?fref=ts");
    }

    public void SoundCloud()
    {
        Instantiate(button_sfx);
        Application.OpenURL("https://soundcloud.com/4knights-game-studio");
    }

    public void Twitter()
    {
        Instantiate(button_sfx);
        Application.OpenURL("https://twitter.com/4knightsgames");
    }

	public void ShareToTwitter()
	{
        Instantiate(button_sfx);
        Application.OpenURL ("https://twitter.com/intent/tweet?text=" + WWW.EscapeURL(Message) + "&amp;lang=en");
	}
    public void PaypalDonate()
    {
        Instantiate(button_sfx);
        Application.OpenURL("https://paypal.me/4knightsEnt");
        //Debug.Log("player is donatation");
    }
}
