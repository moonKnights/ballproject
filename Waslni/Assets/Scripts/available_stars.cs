﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI; // needed for text

public class available_stars : MonoBehaviour {

    /* Show the Value of Available start in UI Text element*/
    int bonce_stars = 0;

    public Text available_star_text;
    public static int total_stars_earned = 0;
    public static int available_star;
    public static bool calc_stars;

    void Start()
    {
        calc_stars = true;
    }

	// Update is called once per frame
	void Update () 
	{
        if (calc_stars)
        {
            total_stars_earned = 0; // reset the value 
            bonce_stars = 0;

            for (int i = 0; i < Game_Data.number_of_lvl; i++)
            {
                total_stars_earned += Game_Data.lvl_star[i];
            }

            // add one bonce star for every perfect level
            for (int i = 0; i < Game_Data.number_of_lvl; i++)
            {
                if (Game_Data.lvl_perfect[i])
                    bonce_stars++;
            }


            for (int i = 0; i < Game_Data.number_of_achi; i++)
            {
                if (Game_Data.achi_unlocked[i])
                {
                    switch (i)
                    {
                        case 0: bonce_stars += 1; break;
                        case 1: bonce_stars += 3; break;
                        case 2: bonce_stars += 5; break;
                        case 3: bonce_stars += 7; break;
                        case 4: bonce_stars += 10; break;
                        case 5: bonce_stars += 15; break;
                        case 6: bonce_stars += 25; break;
                    }
                }
            }

            total_stars_earned += bonce_stars + Game_Data.ads_stars + Game_Data.bought_stars;

            available_star = total_stars_earned - Game_Data.used_stars;

            Debug.Log("Avaliable stars calculation is Done!");
        }

        available_star_text.text = available_star.ToString("D4");
        calc_stars = false;
	}
}
