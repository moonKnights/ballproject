using UnityEngine;
using System.Collections;

public class ObjectMove : MonoBehaviour {

    public float 
        min_position=0.01f,max_position=0.01f, position_step_y = 0.0005f;

    float start_position_y, position_target_y, position_y, position_x;

	// Use this for initialization
	void Start ()
    {
        start_position_y = this.gameObject.transform.transform.position.y;
        //rotation_target = max_rotation + this.gameObject.transform.position.y;
    }
	
	// Update is called once per frame
	void Update ()
    {
        position_y = this.gameObject.transform.transform.position.y;
        position_x = this.gameObject.transform.transform.position.x;

        // Y move
        if (position_target_y >= position_y)
        {
            position_target_y = start_position_y + max_position;
            this.gameObject.transform.position = new Vector3(position_x, position_y +position_step_y,0);

        }

        
        if(position_target_y < position_y)
        {
            position_target_y = start_position_y - min_position;
            this.gameObject.transform.position = new Vector3(position_x,position_y - position_step_y,0);

        }

    }
}
