﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

[Serializable]
public class ItemList
{
    public bool AddItem;
    public string ItemName;

    public ItemList (bool add_item, string item_name)
    {
        AddItem = add_item;
        ItemName = item_name;
    }
}
