﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using admob;
using UnityEngine.Analytics;
using System.Collections.Generic;

public class Shop_System : MonoBehaviour {

    public GameObject Shop_Panel;
    public GameObject Black_BG;
    public GameObject Button_fx;

    public GameObject ColorPicker;

    public GameObject Ulimate_Black;
    public GameObject No_enough_Star;
    public Material PlayerMatM;

    public Text featured, skins, music;
    public GameObject featuredP, skinsP, musicP;
    public static Material PlayerMat;

    public static bool NoEnoughStars = false;
    public static bool ColorPick = false;
    public static bool MusicPick = false;
    public static bool clicksound = false;

    private float show_open_time, shop_close_time;

    public void featuredtab()
    {
        featured.color = new Color(255, 255, 255, 1);
        skins.color = new Color(255, 255, 255, 0.5f);
        music.color = new Color(255, 255, 255, 0.5f);

        featuredP.SetActive(true);
        skinsP.SetActive(false);
        musicP.SetActive(false);

    }

    public void skinstab()
    {
        featured.color = new Color(255, 255, 255, 0.5f);
        skins.color = new Color(255, 255, 255, 1);
        music.color = new Color(255, 255, 255, 0.5f);

        featuredP.SetActive(false);
        skinsP.SetActive(true);
        musicP.SetActive(false);

    }

    public void musictab()
    {
        featured.color = new Color(255, 255, 255, 0.5f);
        skins.color = new Color(255, 255, 255, 0.5f);
        music.color = new Color(255, 255, 255, 1);

        featuredP.SetActive(false);
        skinsP.SetActive(false);
        musicP.SetActive(true);

    }
    public void Show_Shop()
    {
        Instantiate(Button_fx);
        Black_BG.SetActive(true);
        Shop_Panel.SetActive(true);
        PauseMenu.shop_active = true;
        #if UNITY_EDITOR
        Debug.Log("google ads dont work in Editor (Show (Pause))");
#elif UNITY_ANDROID
        Admob.Instance().showBannerRelative(AdSize.Banner, AdPosition.TOP_CENTER, 0);
#endif

        show_open_time = Time.timeSinceLevelLoad;
    }

    public void Back_Shop()
    {
        Instantiate(Button_fx);
        Black_BG.SetActive(false);
        Shop_Panel.SetActive(false);
        PauseMenu.shop_active = false;
#if UNITY_EDITOR
        Debug.Log("google ads dont work in Editor (Remove Resume)");
#elif UNITY_ANDROID
        Admob.Instance().removeBanner();
#endif

        shop_close_time = Time.timeSinceLevelLoad;
        float shopping_time = Mathf.Round( shop_close_time - show_open_time);
        
        // call the costum analytic class function when player close shop
        MyAnalytics.shoptime(shopping_time);

    }

    public void Back_NoEnough()
    {
        Instantiate(Button_fx);
        Ulimate_Black.SetActive(false);
        No_enough_Star.SetActive(false);
    }

    public void Hide_ColorPick()
    {
        Instantiate(Button_fx);
        Ulimate_Black.SetActive(false);
        ColorPicker.SetActive(false);
        Game_Data.Data.Save();
    }

	void Awake()
    {
        PlayerMat = PlayerMatM;

    }

	// Update is called once per frame
	void Update ()
    {
	    if(NoEnoughStars)
        {
            Instantiate(Button_fx);
            Ulimate_Black.SetActive(true);
            No_enough_Star.SetActive(true);
            NoEnoughStars = false;
        }

        if(ColorPick)
        {
            Instantiate(Button_fx);
            Ulimate_Black.SetActive(true);
            ColorPicker.SetActive(true);
            ColorPick = false;
        }

        if(clicksound)
        {
            Instantiate(Button_fx);
            clicksound = false;
        }
	}
}
